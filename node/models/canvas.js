// models/canvas.js
​
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
​
var canvasSchema = new Schema({
    channel: String,
    room: String,
    data: String }
);
​
module.exports = mongoose.model('canvas', canvasSchema);
// models/user.js
​
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
​
var userSchema = new Schema({
    userID: String,
    PW: String,
    auth: [String] }
);
​
module.exports = mongoose.model('user', userSchema);
// routes/userdb.js
​
module.exports = function(app, wss, channels, User, Canvas)
{
    // SIGN UP
    app.post('/user', function(req, res){
        var user = new User();
        user.userID = req.body.userID;
        user.PW = req.body.PW;
        user.auth = [];

        user.save(function(err){
            if(err){
                 console.error(err);
                res.json({result: 0});
                return;
            }

            res.json({result: 1});
        });
    });
​
    // Give AUTH
    app.put('/:userID/AddAuth/:channel', function(req,res){
        User.update({userID: req.params.userID}, {$push: {auth : req.params.channel}}, function(err, output){
            if (err) res.status(500).json({ error: 'database failure' });
            console.log(output);
            if (!output.n) return res.status(404).json({ error: 'user not found'});
            res.json( {message: 'auth added'});
        })
    });
​
    // Delete AUTH
    app.put('/:userID/DeleteAuth/:channel', function(req,res){
        User.update({userID: req.params.userID}, {$pull: {auth : req.params.channel}}, function(err, output){
            if (err) res.status(500).json({ error: 'database failure' });
            console.log(output);
            if (!output.n) return res.status(404).json({ error: 'user not found'});
            res.json( {message: 'auth deleted'});
        })
    });
​
    // CHANGE PASSWORD
    app.put('/PWchange/:userID/:PW/:newPW', function(req,res){
        User.update({userID: req.params.userID, PW: req.params.PW}, {$set: {PW : req.params.newPW}}, function(err, output) {
            if(err) res.status(500).json({ error: 'database failure' });
            console.log(output);
            if(!output.n) return res.status(404).json({ error: 'user not found' });
            res.json( { message: 'password updated' });
        })
    });
​
    // DELETE USER
    app.delete('/deleteuser/:userID/:PW', function(req,res){
        User.remove({ userID: req.params.userID, PW: req.params.PW }, function(err, output){
            if(err) return res.status(500).json({ error: "database failure" });
            console.log(output);
            if(!output.n) return res.status(404).json({ error: 'uesr not found' });

            /* ( SINCE DELETE OPERATION IS IDEMPOTENT, NO NEED TO SPECIFY )
            if(!output.result.n) return res.status(404).json({ error: "book not found" });
            res.json({ message: "book deleted" });
            */

            res.status(204).end();
        })
    });
​
    // GET ALL USERS
    app.get('/user', function(req,res){
        User.find(function(err, users){
            if(err) return res.status(500).send({error: 'database failure'});
            res.json(users);
        })
    });

    // DELETE USER
    app.delete('/user/:user_id', function(req, res){
        User.remove({ _id: req.params.user_id }, function(err, output){
            if(err) return res.status(500).json({ error: "database failure" });
​
        /* ( SINCE DELETE OPERATION IS IDEMPOTENT, NO NEED TO SPECIFY )
        if(!output.result.n) return res.status(404).json({ error: "book not found" });
        res.json({ message: "book deleted" });
        */
​
        res.status(204).end();
        })
    });
}
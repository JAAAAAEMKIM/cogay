// routes/index.js
​
const WebSocket = require('ws');
​
module.exports = function(app, wss, channels, User, Canvas)
{
    // TEST
    app.get('/paintlist', function(req,res){
        res.json({result: 1})
        console.log("paintlist request");
    });
​
    // Get all Channels and Rooms Structure
    app.get('/channel', function (req, res){
        res.json(channels);
    })
​
    // Show Channels
    app.get('/ShowChannel', function (req,res){
        res.json(Object.keys(channels));
    })
​
    // Show Rooms in selected Channel
    app.get('/:channel/ShowRoom', function (req,res){
        res.json(Object.keys(channels[req.params.channel]));
    })
​
    // New Channel
    app.get('/NewChannel/:channelname', function (req,res){
        channels[req.params.channelname] = {};
        res.json("Created channel: " + req.params.channelname);
        console.log("Created channel: " + req.params.channelname);
    })
​
    // New Room
    app.get('/:channel/NewRoom/:roomname', function (req,res){
        channels[req.params.channel][req.params.roomname] = [];
        res.json("Created room in channel: " + req.params.channel + " roomname: " + req.params.roomname);
        console.log("Created room in channel: " + req.params.channel + " roomname: " + req.params.roomname);
    });
​
    // Delete Channel
    app.get('/DeleteChannel/:channelname', function (req,res){
        delete channels[req.params.channelname];
        res.json("Deleted channel: " + req.params.channelname);
        console.log("Deleted channel: " + req.params.channelname);
    })
​
    // Delete Room
    app.get('/:channel/DeleteRoom/:roomname', function (req,res){
        delete channels[req.params.channel][req.params.roomname];
        res.json("Deleted room in channel: " + req.params.channel + " roomname: " + req.params.roomname);
        console.log("Deleted room in channel: " + req.params.channel + " roomname: " + req.params.roomname);
    })
​
    // Websocket
    wss.on('connection', function connection(ws, req) {
        ws.on('message', function incoming(message) {
            message = JSON.parse(message);
            switch(message.event) {
                case 'draw':
                    console.log(channels);
                    channels[message.channel][message.room].forEach(function each(client){
                        if (client !== ws && client.readyState === WebSocket.OPEN) {
                            client.send(message.data);
                    }
                });
                    break;
                case 'first':
                    console.log("Connected");
                    channels[message.channel][message.room].push(ws);
                    break;
                default:
            };
        });
    });
}
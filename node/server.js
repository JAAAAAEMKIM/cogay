// server.js
​
// env
require('dotenv').config();
​
// Dependencies
const express = require('express');
const app = express();
const http = require('http');
const WebSocket = require('ws');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
​
// Initialize channels
var channels = {};
​
// Http Server
const server = http.createServer(app);
// Websocket Server
const wss = new WebSocket.Server( {server} );
// Port
const port = process.env.PORT || 8080;
​
// Configure app to use body-parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
​
// Configure MongoDB Server
var db = mongoose.connection;
db.on('error', console.error);
db.once('open', function(){
    // CONNECTED TO MONGODB SERVER
    console.log("Connected to mongod server");
});
​
mongoose.connect('mongodb://localhost/dbdbdib');
​
// Define Model
var User = require('./models/user');
var Canvas = require('./models/canvas');
​
// Configure Router
var router = require('./routes/index')(app, wss, channels, User, Canvas);
var router = require('./routes/userdb')(app, wss, channels, User, Canvas);
​
// Run Server
server.listen(port, () => {
    console.log('Server started on port' + port);
});
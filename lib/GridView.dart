import 'package:flutter/material.dart';
import 'package:drawing_space/Fileio.dart';
import 'package:dio/dio.dart';
import 'inputform.dart';
import 'dart:convert';
//import 'circular_button.dart';
import 'package:drawing_space/main.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:web_socket_channel/status.dart' as status;
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:share/share.dart';

import 'package:drawing_space/paint.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'http.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';

class GridViewApp extends StatelessWidget {
  final Response res;
  final String channel;
  final String userid;
  String username;
  String channelname;
  final WebSocketChannel listws;
  final WebSocketChannel wschannel = IOWebSocketChannel.connect(DotEnv().env['WS_IP_B']);


  void _sendData(String event) {
    wschannel.sink.add(
        jsonEncode({
          'event': event,
          'channel': channel,
          'room': '',
          'data': '',
          'nickname': username+'|'+userid,
          'page':0,
        })
    );
  }



  Future<bool> _willPopCallback(BuildContext context) async {
    debugPrint('willpop');
    _sendData('leave channel grid');
    wschannel.sink.close();
//    Navigator.of(context).pop();
    // await showDialog or Show add banners or whatever
    // then
    return true; // return true if the route to be popped
  }
  @override
  Widget build(BuildContext context) {

    return new WillPopScope(
        child: new Scaffold(body: new GridList(listws: this.listws, wschannel: wschannel, username: this.username, channelname: this.channelname, res: this.res, channel: this.channel, userid: this.userid)
        ),
        onWillPop: () =>_willPopCallback(context)
    );
  }

  GridViewApp({Key key, this.listws, this.username, this.channelname, @required this.userid, @required this.res, @required this.channel}) : super(key: key);
}

class GridList extends StatefulWidget {
  final WebSocketChannel listws;
  final Response res;
  final String channel;
  final String userid;
  String channelname;
  String username;
  final WebSocketChannel wschannel;


  GridList({Key key, this.listws, this.wschannel, this.username, this.channelname, @required this.res, @required this.channel,@required this.userid}) : super(key: key);

  @override
  createState() => new GridListState();
}


class GridListState extends State<GridList>{
  List<String> Gridlist;
  String nam='';
  Response _res;
  String _userid;
  bool isnull=true;
  String CH_NAME;
  String channel;
  String username;
  MyHttpRequests http = MyHttpRequests();
  List<String> _todoItems=[];
  List<String> _channelhosts=[];
  List<String> _channelhosts_id=[];
  List<String> _channelcodes=[];
  Color color = Colors.white;
  StackExample _stackExample;

  void _sendData(String event, String room) {
    widget.wschannel.sink.add(
        jsonEncode({
          'event': event,
          'channel': channel,
          'room': room,
          'data': '',
          'nickname': username+'|'+widget.userid,
          'page':0,
        })
    );
  }

  void GetChannelListByID(String id) async {
    MyHttpRequests http = MyHttpRequests();
    Response _response;
    _response = await http.makeHttpRequest('showchannel/$id').then((_response) {
      setState(() {
        if(_response.data.length!=0){
          for (var i = 0; i < _response.data.length; ++i) {
            var o = _response.data[i];
            List<String> hostAndChannel = o['channelname'].split('|').toList();

            _todoItems.add(hostAndChannel[1]);
            _channelhosts.add(hostAndChannel[0]);
            _channelhosts_id.add(o['channelcode'].split('|').toList()[0]);
            _channelcodes.add(o['channelcode'].toString());
          }
        }else{

        }
      });
      return null;
    });
  }

  void _connectData(String event, String channel, String username) {
//    debugPrint('websocket: ${widget.wschannel}');
    widget.listws.sink.add(
        jsonEncode({
          'event': event,
          'channel': channel,
          'room': '',
          'data': '',
          'nickname': username, //nickname + id
          'page':0,
        })
    );
  }

  @override
  void initState() {
    super.initState();

    void refreshStack(WebSocketChannel _wschannel, List<String> _gridlist){
      setState(() {

      });
    }

    _stackExample = new StackExample(wschannel: widget.wschannel, gridlist: Gridlist, notify: refreshStack);
    Gridlist = [];
    username = widget.username;
    _userid = widget.userid;
    GetChannelListByID(_userid);
    debugPrint('grid id: $_userid');
    CH_NAME = widget.channelname.split('|')[1];
    channel = widget.channel;
    _res = widget.res;
    debugPrint('NULL? ${_res.data}');
    if(_res.data.length >= 0){
      isnull = false;
      for (var i = 0; i < _res.data.length; ++i) {
        var o = _res.data[i];

        debugPrint('grid data: $o');
        Gridlist.add(o);
      }
      debugPrint('Gridlist: $Gridlist');
    }
    debugPrint('grid id: $_userid, ${widget.userid}');

    _sendData("connect channel grid", '');


}

  @override
  Widget build(BuildContext context) {
    String title = '';
    bool _isnull = isnull;
    TextEditingController _text;
    bool _validate = false;
    String submitStr = "";

    void refreshStack(){
      setState(() {

      });
    }

    void _changeTitle(String val) {
      setState(() {
        title = val;
      });
    }

    _changeTitle(CH_NAME);
    debugPrint('change :$CH_NAME');

    void _changeText(String val) {
      setState(() {
        submitStr = val;
      });

      print("On RaisedButton : The text is $submitStr");
    }

    void _onSubmit(String val) {
      print("OnSubmit : The text is $val");
      setState(() {
        submitStr = val;
      });
    }

    void _onChanged(String value) {
      print('"OnChange : " $value');
    }

    void _sendData(String event, String room) {
      widget.wschannel.sink.add(
          jsonEncode({
            'event': event,
            'channel': channel,
            'room': room,
            'data': '',
            'nickname': username+'|'+widget.userid,
            'page':0,
          })
      );
    }
    void _changeData(String event, String _origin, String _new) {
      widget.wschannel.sink.add(
          jsonEncode({
            'event': event,
            'originchannel': _origin,
            'newchannel': _new,
            'room': '',
            'data': '',
            'nickname': username+'|'+widget.userid,
            'page':0,
          })
      );
    }

    return MaterialApp(
      title: title,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          automaticallyImplyLeading: false,
          leading: IconButton(icon:Icon(Icons.arrow_back),
            onPressed:() {Navigator.pop(context, false); _sendData('leave channel grid', '');
            widget.wschannel.sink.close();},
            color: Colors.black,
          ),
            //`true` if you want Flutter to automatically add Back Button when needed,
          title: Text(title, textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
          centerTitle: true,
          actions: <Widget>[
//            StackExample(),
          ],


        ),
        floatingActionButton:
          Stack(children:<Widget>[
            SpeedDial(
              animatedIcon: AnimatedIcons.list_view,
              animatedIconTheme: IconThemeData(size: 22.0),
              // this is ignored if animatedIcon is non null
              // child: Icon(Icons.add),
              //          visible: _dialVisible,
              curve: Curves.bounceIn,
              overlayColor: Colors.black,
              overlayOpacity: 0.5,
              onOpen: () => print('OPENING DIAL'),
              onClose: () => print('DIAL CLOSED'),
              tooltip: 'Speed Dial',
              heroTag: 'speed-dial-hero-tag',
              backgroundColor: Colors.white,
              foregroundColor: Colors.black,
              elevation: 8.0,
              shape: CircleBorder(),
              children: [
                SpeedDialChild(
                  child: Icon(Icons.add_box),
                  backgroundColor: Colors.orangeAccent,
                  label: '새 노트',
                  //                labelStyle: TextTheme(fontSize: 18.0),
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) =>
                        InputForm(userid: _userid, channel: channel, index: 1000008, items: Gridlist, notifyParent: refreshAdd)

                );//edit name

              },
            ),

              SpeedDialChild(
                child: Icon(Icons.group_add),
                backgroundColor: Colors.deepOrangeAccent,
                label: '이 채널에 초대',
                //              labelStyle: TextTheme(fontSize: 18.0),
                onTap: () {
                  String text = widget.channel+'/'+widget.channelname;
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return new
                        AlertDialog(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(Radius.circular(24.0))),
                          content: Container(height: 160, child: Column(children: <Widget>[Text("채널에 초대하기", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),SizedBox(height: 30),Text(text),IconButton(icon: Icon(Icons.share), onPressed:(){ Share.share(text);})]),)

                      );
                    });
                  },
                ),
              ],
            ),
          ],
        ),StackExample(wschannel: widget.wschannel, gridlist: Gridlist),]),
        body :
          Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
            Expanded(
                flex:3,
              child:SizedBox(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width*2/3,
                child:Container(
                  color: Color.fromRGBO(240, 255, 255,0.7),
                  child: GridView.builder(
                      itemCount: Gridlist.length,
                      gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
                      itemBuilder: (context, index) {
                        // ignore: missing_return
                        if(index < Gridlist.length) {
                          return new Container(
                              child: new ListTile(
                                  title: Stack(children: <Widget>[
                                    Card(
                                        shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(24.0))),
                                        color: Color.fromRGBO(252, (230-index*13), 110+index*10, 1),
                                        child:Container(
                                            height: MediaQuery.of(context).size.height/5.6,
                                                child:Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: <Widget>[
                                                    Container(
                                                      child: Padding(
                                                        padding: EdgeInsets.fromLTRB(0, 40, 0, 8),
                                                        child: Column(
                                                          crossAxisAlignment: CrossAxisAlignment.center,
                                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        ),
                                                      ),
                                                  ),
                                                ],
                                            )
                                        ),
                                      elevation: 5),
                                    Column(
                                        mainAxisAlignment:MainAxisAlignment.end,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[Card(
                                          shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(Radius.circular(24.0))),
                                          elevation: 0,
                                          child: Container(
                                            height: MediaQuery.of(context).size.height*(1/6),
                                              child:
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: <Widget>[
                                                Container(
                                                  child: Padding(
                                                    padding: EdgeInsets.fromLTRB(0, 0, 0, 8),
                                                    child:
                                                    Column(
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                      mainAxisSize: MainAxisSize.min,
                                                      children: <Widget>[
                                                          Container(height: MediaQuery.of(context).size.width*(1/15) ,),
                                                          Container(
                                                              child: Flexible(
                                                                  child: Container(
                                                                    width: MediaQuery.of(context).size.width*(1/6),
                                                                    height: MediaQuery.of(context).size.width*(1/8),
                                                                    child:  Text(
                                                                      Gridlist[index], style: TextStyle(fontSize: 20), overflow: TextOverflow.fade,maxLines: 2,textAlign: TextAlign.center,),
                                                                  )

                                                            )
                                                          ),
                                                          Container(child:
                                                              Icon(Icons.arrow_drop_down),
                                                        )
                                                      ],
                                                      ),
                                                    ),
                                                ),
                                                ],
                                              )
                                            ),
                                        ),
              //
                                      ],),
                                      ],),
                                onLongPress: (){
                                    String ROOM = Gridlist[index];
                                  showDialog(
                                      context: context,
                                      builder: (_) => new AlertDialog(
                                          title: new Text('select action'),
                                          actions: <Widget> [new Column(
                                              mainAxisSize: MainAxisSize.min,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: <Widget>[
                                                new RaisedButton(
                                                  child: new Text("Edit room"),
                                                  onPressed: () async {

                                                    Navigator.of(context, rootNavigator: true).pop('dialog');
                                                    showDialog(
                                                        context: context,

                                                        builder:(_)=> new Container(
                                                            child:AlertDialog(
                                                                content: new Column(
                                                                    mainAxisSize: MainAxisSize.min,
                                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                                    children: <Widget>[
                                                                      Text('Input New Name'),
                                                                      TextField(
                                                                        controller: _text = TextEditingController(text: Gridlist[index]),
                                                                        autofocus: true,
                                                                        decoration: new InputDecoration(
                                                                          hintText: 'Enter something...',
                                                                          contentPadding: const EdgeInsets.all(16.0),
                                                                          errorText: _validate ? 'Name already exists.' : null,
                                                                        ),
                                                                        onChanged: (String value) {
                                                                          _onChanged(value);
                                                                        },
                                                                        onSubmitted: (String submittedStr) {
                                                                          _onSubmit(submittedStr);
                                                                          _text.text = "";
                                                                        },

                                                                      ),
                                                                      new Text('$submitStr'),
                                                                      new RaisedButton(
                                                                          child: new Text("Click me"),
                                                                          onPressed: () {
                                                                            _changeText(_text.text);
                                                                            String val = _text.text;

                                                                            Navigator.of(context, rootNavigator: true).pop('dialog');
                                                                            if(checkval(val, Gridlist)){
                                                                              http.putHttpRequest('editroom/${widget.channel}/$ROOM/$val');
                                                                              debugPrint('ROOM edit: editroom/$_userid|$CH_NAME/$ROOM/$val');

                                                                              setState(() {
                                                                                Gridlist.removeAt(index);
                                                                                Gridlist.insert(index, val);
                                                                              });
                                                                            }
                                                                            else{
                                                                              _validate=true;
                                                                            }
                                                                          }

                                                                      )
                                                                    ]
                                                                )
                                                            )
                                                        )
                                                    );
                                                  },
                                                  textColor: Colors.white,
                                                  color: Colors.blueAccent,
                                                ),
                                                new RaisedButton(
                                                  child: new Text("delete room"),
                                                  onPressed: () async {
                                                    await http.deleteHttpRequest('/deleteroom/$channel/$ROOM');
                                                    await http.makeHttpRequest('showroom/$channel').then((tmp){
                                                      setState(() {
                                                        Gridlist = [];
                                                        for (var i = 0; i < tmp.data.length; ++i) {
                                                          var o = tmp.data[i];
                                                          Gridlist.add(o);
                                                        }
                                                        Navigator.of(context, rootNavigator: true).pop('dialog');
                                                      });
                                                    });
                                                  },
                                                  textColor: Colors.white,
                                                  color: Colors.blueAccent,
                                                )
                                              ]
                                          )
                                          ]
                                      ));
                                },
                                onTap: () async {
                                  //                              _sendData(event)
                                  String ROOM = Gridlist[index];
                                  debugPrint('$ROOM, $CH_NAME');
                                  debugPrint("gridview ");
                                  _sendData('connect room grid', '$ROOM');
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => Draw(username: username, room : ROOM, channelname: widget.channelname, channel_: widget.channel, userid: _userid, wsgrid: widget.wschannel)));
                                  debugPrint("router");
                                },
                              )
                          );
                        }

                        else return null;
                      }
                  ),
                )
              )
            ),


          ],
        ),
        drawer: SizedBox(
          width: MediaQuery.of(context).size.width*3/5,
          child:  Drawer(
              child: SizedBox(
                height: MediaQuery.of(context).size.height,
                width: 10,
                child:
                Container(
                  width:10,
                  child:
                  ListView.builder(
                      itemCount: _channelcodes.length,
                      itemBuilder: (context, index) {
                        if(index < _todoItems.length) {
                          return ListTileTheme(
                              selectedColor: Colors.red,
                              style: ListTileStyle.drawer,
                              child:new Container(
                                //color: Colors.white,
                                width: 10,
                                //                  key: Key(_channelcodes[index]),
                                child:ListTile(
                                  onTap: () async {
                                    String CHANNEL_CODE = _channelcodes[index]; //ch_code = id|ch_name //
                                    _changeData('change channel grid', channel, CHANNEL_CODE);
//                          _connectData("connect channel main", _channelcodes[index], widget.username+'|'+_userid);
                                    Response res =  await http.makeHttpRequest('showroom/$CHANNEL_CODE').then((res){
                                      setState(() {
                                        color = Colors.grey;//
                                        Gridlist = [];
                                        channel = _channelcodes[index];
                                        for (var i = 0; i < res.data.length; ++i) {
                                          var o = res.data[i];

                                          Gridlist.add(o);
                                        }
                                        CH_NAME = _todoItems[index];
                                        _changeTitle(_todoItems[index]);
                                      });
                                      return null;
                                    });
                                  },
                                  title: Column(
                                    children: <Widget>[

                                      Container(
                                        height: 50,
                                        child:
                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                              Row(
                                                mainAxisSize: MainAxisSize.min,
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: <Widget>[
                                                  Container(
                                                      color: Color.fromRGBO(252,  (120+index/(_todoItems.length)*130).toInt(), 121, 1),
                                                      width: MediaQuery.of(context).size.width/20
                                                  ),
                                                  Container(
                                                      width: MediaQuery.of(context).size.width*1/30
                                                  ),
                                                  Flexible(
                                                    child: Container(
                                                      width: MediaQuery.of(context).size.width*3/10,
                                                      child: Padding(
                                                          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                                          child:  Text(
                                                          _todoItems[index],
                                                          overflow: TextOverflow.ellipsis,
                                                            maxLines: 1,
                                                        ),
                                                      ),

                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Icon(Icons.arrow_forward_ios, color: Colors.grey,),


                                            ],)

                                      ),
                                      Divider(
                                      )
                                    ],
                                  ),


                                ),
                              ));
                        }
                        else return null;
                      }),
                ),
              )),
        ),
      ),

      );
  }

  bool checkval(String val, List<String> list){
    bool answer = true;
    if(val.length==0){
      answer = false;
    }
    else if(ValisinList(val, list)){
      answer = false;
    }
    debugPrint('answer:$answer');
    return answer;
  }

  bool ValisinList(String val, List<String> list){
    bool answer = false;
    for (var i = 0; i < list.length; ++i) {
      var o = list[i];

      if(val == o){
        answer = !answer;
      }
    }
    return answer;
  }

  refreshAdd(String val, int index)async{
    await http.makeHttpRequest('showroom/$channel').then((tmp){
      setState(() {
        Gridlist = [];
        for (var i = 0; i < tmp.data.length; ++i) {
          var o = tmp.data[i];
          Gridlist.add(o);
        }
      });
    });
  }
}

class StackExample extends StatefulWidget {
  final WebSocketChannel wschannel;
  Stream stream;
  List<String> gridlist;
  final Function notify;

  StackExample({Key key, this.wschannel, this.gridlist, this.notify}) : super(key: key);


  @override
  createState() => new StackExampleState();
}

class StackExampleState extends State<StackExample>{
  List<String> users;
  int _length ;

  @override
  void initState() {
    super.initState();
    users = [];
    _length = 0;
//    _length = users.length;
  }

  @override
  Widget build(BuildContext context) {

    Future<List<String>> waitsnapshot(AsyncSnapshot snapshot) async {
      await jsonDecode(snapshot.data)['users'].then((list){
        setState((){
          users = list;
          debugPrint('userlist: $list');
        });
      });
      return users;
    }



    debugPrint("mediaquery ${MediaQuery.of(context).size.width*8/9}");
    return new Container(
        padding: const EdgeInsets.all(8.0),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        // alignment: FractionalOffset.center,
//        child:new StreamBuilder(
//          stream: widget.wschannel.stream,
//          builder: (context, snapshot) {
//            waitsnapshot(snapshot).then((tmp){
//              debugPrint('stream2: $tmp');
//              setState(() {
////                users = tmp;
//                _length = users.length;
//              });
//            });
//            debugPrint('length: $_length');
//            if(_length==0){
//              return new Stack(
//                  alignment:new Alignment(0, 0),
//                  children: <Widget>[
//
//                    new Positioned(
//                        left: 20,//
//                        bottom: 50,
//                        child: new Text("Users")
//                    ),
//                  ]
//              );
//            }
//
//            else if(_length==1){
//              return new Stack(
//                  alignment:new Alignment(0, 0),
//                  children: <Widget>[
//
//                    new Positioned(
//                        left: 20,//
//                        bottom: 50,
//                        child: new Text("Users")
//                    ),
//                    new Positioned(
//                      left: 20,//
//                      bottom: 0,
//                      child: new CircleAvatar(backgroundColor: Colors.orangeAccent,radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 17, child: new Text('김')))
//                    ),
//
//                  ]
//              );
//            }
//            else if(_length==2){
//              return new Stack(
//                  alignment:new Alignment(0, 0),
//                  children: <Widget>[
//
//                    new Positioned(
//                        left: 20,//
//                        bottom: 50,
//                        child: new Text("Users")
//                    ),
//                    new Positioned(
//                      left: 20,//
//                      bottom: 0,
//                      child: new CircleAvatar(backgroundColor: Colors.orangeAccent,radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 17, child: new Text('김')))
//                    ),
//                    new Positioned(
//                        left: 45,
//                        bottom: 0,
//                        child: new CircleAvatar(backgroundColor: Colors.orange,radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 17, child: new Text('재')))
//                    ),
//
//                  ]
//              );
//            }
//            else{ //length >=3
//              return new Stack(
//                  alignment:new Alignment(0, 0),
//                  children: <Widget>[
//
//                    new Positioned(
//                        left: 20,//
//                        bottom: 50,
//                        child: new Text("Users")
//                    ),
//                    new Positioned(
//                        left: 20,//
//                        bottom: 0,
//                        child: new CircleAvatar(backgroundColor: Colors.orangeAccent,radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 17, child: new Text('김')))
//                    ),
//                    new Positioned(
//                        left: 45,
//                        bottom: 0,
//                        child: new CircleAvatar(backgroundColor: Colors.orange,radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 17, child: new Text('재')))
//                    ),
//                    new Positioned(
//                        left: 70,
//                        bottom: 0,
//                        child: new CircleAvatar(backgroundColor: Colors.deepOrangeAccent,radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 17, child: new Text('민')))
//                    ),
//
//                  ]
//              );
//            }
////            else{return new Stack(
////                alignment:new Alignment(0, 0),
////                children: <Widget>[
////
////            new Positioned(
////            left: 20,//
////                bottom: 50,
////                child: new Column(children:<Widget>[new Text("Users"),new Text("None")])
////            )]);}
//        },
//      )
    );

  }
}
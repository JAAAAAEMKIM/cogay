import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'main.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:collection/collection.dart';
import 'package:flutter/services.dart';
import 'package:clock/clock.dart';
import 'dart:convert';
import 'package:drawing_space/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:dio/dio.dart';


import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:web_socket_channel/status.dart' as status;

class FacebookLoginPageApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new Container(
        child: new FacebookLoginPage()
    );
  }
}

class FacebookLoginPage extends StatefulWidget {
//로그인 확인은 login.dart에서 진행 후 로그인으로 진행할 지 말 지 결정.


  @override
  FacebookLoginPageState createState() => new FacebookLoginPageState();
}

class FacebookLoginPageState extends State<FacebookLoginPage> with TickerProviderStateMixin {
  static final FacebookLogin facebookSignIn = new FacebookLogin();
  Dio dio = new Dio();
  MyHttpRequests _http = MyHttpRequests();
  String _message = 'Log in/out by pressing the buttons below.';
  String _id = '';
  String _username;
  String _email;

  PageController _pageController;
  AnimationController _submitAnimationController;

  @override
  void initState() {
    super.initState();

    debugPrint("로그인 페이지");
    _pageController = PageController();
    _submitAnimationController =
    AnimationController(vsync: this, duration: Duration(seconds: 0))
      ..addStatusListener(
        (status) async {
          if (status == AnimationStatus.completed) {
//            _goToResultPage(_id);
          }
        },
      );
  }

  _goToResultPage(String id) async {
    return Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => MyMainPage(id: id, username:_username),
      ),
    );
  }

  Future<Null> _login() async {
    final FacebookLoginResult result =
      await facebookSignIn.logInWithReadPermissions(['email','user_status']);
    await DotEnv().load('.env');
    final prefs = await SharedPreferences.getInstance();

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final FacebookAccessToken accessToken = result.accessToken;
        _showMessage('''
         Logged in!
         
         Token: ${accessToken.token}
         Username: ${accessToken}
         User id: ${accessToken.userId}
         Expires: ${accessToken.expires}
         Permissions: ${accessToken.permissions}
         Declined permissions: ${accessToken.declinedPermissions}
         ''');
        prefs.setString('ftoken', accessToken.token);
        prefs.setString('fid', accessToken.userId);

        prefs.setStringList('fPermissions', accessToken.permissions);
        prefs.setStringList('fDecline', accessToken.declinedPermissions);

//        String _token = (prefs.getString('ftoken') ?? 'dongja');

        _id = accessToken.userId;
        debugPrint("post: ${accessToken.userId}");
        _submitAnimationController.forward();
//        var token = result.accessToken.token;
        String hostUrl =
            'https://graph.facebook.com/v2.12/me?fields=name,email,permissions&access_token=${accessToken.token}';
        //https://graph.facebook.com/v2.12/me?fields=name,email,permissions&access_token=EAAHOJSbG8yUBAFjHagVpGQAykCMB0gLTv4syEpBFP6m8aewGbVCRXxtbPzcuZAQr9zvxfOaMD0l6gysgB6FKXtpFR8vfv6boalupYT3fbK6SHQPqHYI8CTge861ZAWDgrO2oMjN2Vz2gAquNkwQos5DopW9KLhSux5JJ1JwwwhbON816MkuhSJOg6hrzBJojmgrD0H4AZDZD

        var res = await dio.get(hostUrl);
        debugPrint("result: ${res.data}");
        var decoded = json.decode(res.data);
        await prefs.setString('fname', decoded['name']);
        debugPrint('pref name ${prefs.getString('fname')}');
        _username = decoded['name'];
//        WebSocketChannel wschannel = IOWebSocketChannel.connect(DotEnv().env['WS_IP_']);

        if (res.statusCode == 200) {
          debugPrint('facebook id: $_id');
          debugPrint("name: ${decoded['name']}");
//          debugPrint("login wschannel: ${wschannel}");

          _http.postHttpRequest('/user', '{"userID": "${accessToken.userId}", "nickname": "$_username"}');
          MaterialPageRoute(
            builder: (context) => MyMainPage(id: _id, username:_username),
          );
          _goToResultPage(_id);
        } else {
          debugPrint("로그인 실패");
        }
        break;

      case FacebookLoginStatus.cancelledByUser:
        _showMessage('Login cancelled by the user.');
        break;
      case FacebookLoginStatus.error:
        _showMessage('Something went wrong with the login process.\n'
            'Here\'s the error Facebook gave us: ${result.errorMessage}');
        break;
    }
  }



  Future<String> setUsername() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    debugPrint('prefs: ${prefs.getString('fname')}');
    String username = await prefs.getString('fname');
    debugPrint('setUsername: $username');
    return username;
  }

  Future<Null> _logOut() async {
    await facebookSignIn.logOut();
    _showMessage('Logged out.');
  }

  void _showMessage(String message) {
    setState(() {
      _message = message;
    });
  }

//  final accessToken = await facebookSignIn.currentAccessToken;
//  bool tokenisalive;

//  void isValid() async {
//    tokenisalive = await facebookSignIn.isLoggedIn;
//  }

  @override
  Widget build(BuildContext context) {
//  isValid();

    debugPrint("로그인 페이지 빌드 중");
    return
//      tokenisalive
//        ?
    new MaterialApp(
      home: new Scaffold(
        backgroundColor: Colors.white,
        appBar: new AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          toolbarOpacity: 1,
        ),
        body: new Center(
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Image.asset('assets/templogo1.png'),
              SizedBox(
                height: MediaQuery.of(context).size.height/20,
              ),
//              new Text(_message),
              new Material(
                //borderRadius: BorderRadius.circular(30.0),
                elevation: 5.0,
                child: MaterialButton(
                  minWidth: MediaQuery.of(context).size.width*2/3,
                  height: MediaQuery.of(context).size.height/20,
                  color: Colors.deepOrangeAccent,
                  onPressed: _login,
                  child: new Text('Continue with Facebook', style:TextStyle(fontSize: 24, color: Colors.white)),
                )
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height/20,
              ),
//              new RaisedButton(
//                onPressed: _logOut,
//                child: new Text('Logout'),
//              ),
            ],
          ),
        ),
      ),
    );}
//        :
//    Navigator.of(context).pushReplacement(
//      MaterialPageRoute(
//        builder: (context) => MyMainPage(id: _id),
//      ),
//    );
//  }
}

class FacebookLogin {
  static const channel = MethodChannel('com.roughike/flutter_facebook_login');

  FacebookLoginBehavior _loginBehavior =
//      FacebookLoginBehavior.nativeWithFallback;
      FacebookLoginBehavior.nativeWithFallback;

  /// Controls how the login dialog should be presented.
  ///
  /// For example, setting this to [FacebookLoginBehavior.webViewOnly] will
  /// render the login dialog using a WebView.
  ///
  /// NOTE: Updating the login behavior won't do anything immediately; the value
  /// is taken into account just before the login dialog is about to show.
  set loginBehavior(FacebookLoginBehavior behavior) {
    assert(behavior != null, 'The login behavior cannot be null.');
    _loginBehavior = behavior;
  }

  /// Returns whether the user is currently logged in and the access token is
  /// still valid or not.
  ///
  /// Convenience method for checking if the [currentAccessToken] is null and not
  /// expired.
  Future<bool> get isLoggedIn async =>
      (await currentAccessToken)?.isValid() ?? false;

  /// Retrieves the current access token for the application.
  ///
  /// This could be useful for logging in the user automatically in the case
  /// where you don't persist the access token in your Flutter app yourself.
  ///
  /// For example:
  ///
  /// ```dart
  /// final accessToken = await facebookLogin.currentAccessToken;
  ///
  /// if (accessToken != null && accessToken.isValid()) {
  ///   _fetchFacebookNewsFeed(accessToken);
  /// } else {
  ///   _showLoginRequiredUI();
  /// }
  /// ```
  ///
  /// NOTE: This might return an access token that has expired. If you need to be
  /// sure that the token is still valid, call [isValid] on the access token.
  Future<FacebookAccessToken> get currentAccessToken async {
    final Map<dynamic, dynamic> accessToken =
    await channel.invokeMethod('getCurrentAccessToken');

    if (accessToken == null) {
      return null;
    }

    return FacebookAccessToken.fromMap(accessToken.cast<String, dynamic>());
  }

  /// Logs the user in with the requested read permissions.
  ///
  /// This will throw an exception from the native side if the [permissions]
  /// list contains any permissions that are not classified as read permissions.
  ///
  /// Returns a [FacebookLoginResult] that contains relevant information about
  /// the current login status. For sample code, see the [FacebookLogin] class-
  /// level documentation.
  Future<FacebookLoginResult> logInWithReadPermissions(
      List<String> permissions,
      ) async {
    final Map<dynamic, dynamic> result =
    await channel.invokeMethod('loginWithReadPermissions', {
      'behavior': _currentLoginBehaviorAsString(),
      'permissions': permissions,
    });

    return _deliverResult(
        FacebookLoginResult._(result.cast<String, dynamic>()));
  }

  /// Logs the user in with the requested publish permissions.
  ///
  /// This will throw an exception from the native side if the [permissions]
  /// list contains any permissions that are not classified as read permissions.
  ///
  /// If called right after receiving a result from [logInWithReadPermissions],
  /// this method may fail. It is recommended to call this method right before
  /// needing a specific publish permission, in a context where it makes sense
  /// to the user. For example, a good place to call this method would be when
  /// the user is about to post something to Facebook by using your app.
  ///
  /// Returns a [FacebookLoginResult] that contains relevant information about
  /// the current login status. For sample code, see the [FacebookLogin] class-
  /// level documentation.
  Future<FacebookLoginResult> loginWithPublishPermissions(
      List<String> permissions,
      ) async {
    final Map<dynamic, dynamic> result =
    await channel.invokeMethod('loginWithPublishPermissions', {
      'behavior': _currentLoginBehaviorAsString(),
      'permissions': permissions,
    });

    return _deliverResult(
        FacebookLoginResult._(result.cast<String, dynamic>()));
  }

  /// Logs the currently logged in user out.
  ///
  /// NOTE: On iOS, this behaves in an unwanted way. As far the Login SDK is
  /// concerned, the access token and session is cleared upon logging out.
  /// However, when using [FacebookLoginBehavior.webOnly], the WKViewController
  /// managed by Safari remembers the user indefinitely.
  ///
  /// This blocks the user from logging in with any other account than the one
  /// they used the first time. This same issue is also present when using
  /// [FacebookLoginBehavior.nativeWithFallback] in the case where the user
  /// doesn't have a native Facebook app installed.
  ///
  /// Using [FacebookLoginBehavior.webViewOnly] resolves this issue.
  ///
  /// For more, see: https://github.com/roughike/flutter_facebook_login/issues/4
  Future<void> logOut() async => channel.invokeMethod('logOut');

  String _currentLoginBehaviorAsString() {
    assert(_loginBehavior != null, 'The login behavior was unexpectedly null.');

    switch (_loginBehavior) {
      case FacebookLoginBehavior.nativeWithFallback:
        return 'nativeWithFallback';
      case FacebookLoginBehavior.nativeOnly:
        return 'nativeOnly';
      case FacebookLoginBehavior.webOnly:
        return 'webOnly';
      case FacebookLoginBehavior.webViewOnly:
        return 'webViewOnly';
    }

    throw StateError('Invalid login behavior.');
  }

  /// There's a weird bug where calling Navigator.push (or any similar method)
  /// straight after getting a result from the method channel causes the app
  /// to hang.
  ///
  /// As a hack/workaround, we add a new task to the task queue with a slight
  /// delay, using the [Future.delayed] constructor.
  ///
  /// For more context, see this issue:
  /// https://github.com/roughike/flutter_facebook_login/issues/14
  Future<T> _deliverResult<T>(T result) {
    return Future.delayed(const Duration(milliseconds: 500), () => result);
  }
}

/// Different behaviors for controlling how the Facebook Login dialog should
/// be presented.
enum FacebookLoginBehavior {
  /// Login dialog should be rendered by the native Android or iOS Facebook app.
  ///
  /// If the user doesn't have a native Facebook app installed, this falls back
  /// to using the web browser based auth dialog.
  ///
  /// This is the default login behavior.
  ///
  /// Might have logout issues on iOS; see the [FacebookLogin.logOut] documentation.
  nativeWithFallback,

  /// Login dialog should be rendered by the native Android or iOS Facebook app
  /// only.
  ///
  /// If the user hasn't installed the Facebook app on their device, the
  /// login will fail when using this behavior.
  ///
  /// On iOS, this behaves like the [nativeWithFallback] option. This is because
  /// the iOS Facebook Login SDK doesn't support the native-only login.
  nativeOnly,

  /// Login dialog should be rendered by using a web browser.
  ///
  /// Might have logout issues on iOS; see the [FacebookLogin.logOut] documentation.
  webOnly,

  /// Login dialog should be rendered by using a WebView.
  webViewOnly,
}

/// The result when the Facebook login flow has completed.
///
/// The login methods always return an instance of this class, whether the
/// user logged in, cancelled or the login resulted in an error. To handle
/// the different possible scenarios, first see what the [status] is.
///
/// To see a comprehensive example on how to handle the different login
/// results, see the [FacebookLogin] class-level documentation.
class FacebookLoginResult {
  /// The status after a Facebook login flow has completed.
  ///
  /// This affects the [accessToken] and [errorMessage] variables and whether
  /// they're available or not. If the user cancelled the login flow, both
  /// [accessToken] and [errorMessage] are null.
  final FacebookLoginStatus status;

  /// The access token for using the Facebook APIs, obtained after the user has
  /// successfully logged in.
  ///
  /// Only available when the [status] equals [FacebookLoginStatus.loggedIn],
  /// otherwise null.
  final FacebookAccessToken accessToken;

  /// The error message when the log in flow completed with an error.
  ///
  /// Only available when the [status] equals [FacebookLoginStatus.error],
  /// otherwise null.
  final String errorMessage;

  FacebookLoginResult._(Map<String, dynamic> map)
      : status = _parseStatus(map['status']),
        accessToken = map['accessToken'] != null
            ? FacebookAccessToken.fromMap(
          map['accessToken'].cast<String, dynamic>(),
        )
            : null,
        errorMessage = map['errorMessage'];

  static FacebookLoginStatus _parseStatus(String status) {
    switch (status) {
      case 'loggedIn':
        return FacebookLoginStatus.loggedIn;
      case 'cancelledByUser':
        return FacebookLoginStatus.cancelledByUser;
      case 'error':
        return FacebookLoginStatus.error;
    }

    throw StateError('Invalid status: $status');
  }
}

/// The status after a Facebook login flow has completed.
enum FacebookLoginStatus {
  /// The login was successful and the user is now logged in.
  loggedIn,

  /// The user cancelled the login flow, usually by closing the Facebook
  /// login dialog.
  cancelledByUser,

  /// The Facebook login completed with an error and the user couldn't log
  /// in for some reason.
  error,
}

/// The access token for using Facebook APIs.
///
/// Includes the token itself, along with useful metadata about it, such as the
/// associated user id, expiration date and permissions that the token contains.
class FacebookAccessToken {
  /// The access token returned by the Facebook login, which can be used to
  /// access Facebook APIs.
  final String token;

  /// The id for the user that is associated with this access token.
  final String userId;

  /// The date when this access token expires.
  final DateTime expires;

  /// The list of accepted permissions associated with this access token.
  ///
  /// These are the permissions that were requested with last login, and which
  /// the user approved. If permissions have changed since the last login, this
  /// list might be outdated.
  final List<String> permissions;

  /// The list of declined permissions associated with this access token.
  ///
  /// These are the permissions that were requested, but the user didn't
  /// approve. Similarly to [permissions], this list might be outdated if these
  /// permissions have changed since the last login.
  final List<String> declinedPermissions;

  /// Is this access token expired or not?
  ///
  /// If the access token has not been expired yet, returns true. Otherwise,
  /// returns false.
  Clock clock = Clock();
  bool isValid() => clock.now().isBefore(expires);

  /// Constructs a access token instance from a [Map].
  ///
  /// This is used mostly internally by this library.
  FacebookAccessToken.fromMap(Map<String, dynamic> map)
      : token = map['token'],
        userId = map['userId'],
        expires = DateTime.fromMillisecondsSinceEpoch(
          map['expires'],
          isUtc: true,
        ),
        permissions = map['permissions'].cast<String>(),
        declinedPermissions = map['declinedPermissions'].cast<String>();

  /// Transforms this access token to a [Map].
  ///
  /// This is used mostly internally by this library.
  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'token': token,
      'userId': userId,
      'expires': expires.millisecondsSinceEpoch,
      'permissions': permissions,
      'declinedPermissions': declinedPermissions,
    };
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is FacebookAccessToken &&
              runtimeType == other.runtimeType &&
              token == other.token &&
              userId == other.userId &&
              expires == other.expires &&
              const IterableEquality().equals(permissions, other.permissions) &&
              const IterableEquality().equals(
                declinedPermissions,
                other.declinedPermissions,
              );

  @override
  int get hashCode =>
      token.hashCode ^
      userId.hashCode ^
      expires.hashCode ^
      permissions.hashCode ^
      declinedPermissions.hashCode;
}
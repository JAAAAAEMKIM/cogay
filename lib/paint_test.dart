import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'dart:ui';
import 'dart:convert';
import 'dart:collection';
import 'package:dio/dio.dart';
import 'package:drawing_space/GridView.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:photo_view/photo_view.dart';
import 'package:drawing_space/image_picker_handler.dart';
import 'package:drawing_space/image_picker_dialog.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:web_socket_channel/status.dart' as status;
import 'package:path_drawing/path_drawing.dart';
import 'package:photo_view/photo_view.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'Fileio.dart';
import 'http.dart';
import 'pagebodyTest.dart';


class DrawTest extends StatefulWidget {

  DrawTest({Key key}): super(key: key);

  @override
  _DrawTestState createState() => _DrawTestState();
}


class _DrawTestState extends State<DrawTest> with TickerProviderStateMixin {

  Color selectedColor = Colors.black;
  Color pickerColor = Colors.black;

  bool showBottomList = false;
  bool showBottomNavigationBar = false;
  bool colorChoose = false;
  bool paperChoose = false;

  double scrollableButtonHeight =60;
  double strokeWidth = 3.0;
  double opacity = 1.0;
  StrokeCap strokeCap = StrokeCap.round;
  SelectedMode selectedMode = SelectedMode.StrokeWidth;
  String user_id;
  IOWebSocketChannel channel;



  List<Color> colors = [Colors.red, Colors.green, Colors.blue, Colors.amber, Colors.black];
  List<Color> buttonColors = [Colors.white,Colors.white,Colors.white,Colors.white, Colors.white, Colors.white,Colors.white];
  List<PageBodyTest> pageBodyList;

  int currentIndex = 0;
  int newIndex;
  AnimationController _controller;
  int pageIndex = 0;
  List<PageBodyTest> pages = List();
  final controller = PageController(initialPage: 1);

  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    BackButtonInterceptor.add(myInterceptor2);

    pageBodyList = [ PageBodyTest()];
//    channel = widget.channel;
//    _sendData("first");
//    debugPrint('first');
//    channel.stream.listen((data) {
//      decode(data);
//      setState((){});
//    });
  }

  @override
  void dispose() {
    _controller.dispose();
//    widget.wschannel.sink.close();

//backbutton
    BackButtonInterceptor.remove(myInterceptor2);

    super.dispose();
  }



  void navigateBack() async{
    BackButtonInterceptor.remove(myInterceptor2);

    BackButtonInterceptor.remove(myInterceptor2);

    BackButtonInterceptor.removeAll();
  }

  bool myInterceptor2 (bool stopDefaultButtonEvent){
//    BackButtonInterceptor.remove(myInterceptor);
    BackButtonInterceptor.removeAll();
    print("BACK BUTTON!"); // Do some stuff.
    debugPrint("backbutton pressed");

//    GetResponse();
    navigateBack();
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
//      persistentFooterButtons: scrollableButtons(),
      bottomNavigationBar: bottomNavigationBar(),      // color를 선택하지 않을 떄
      body: pageBody(),
      extendBody: true,
    );
  }

  Widget bottomNavigationBar(){
    if(colorChoose) return colorChooseBar();
    else if(paperChoose) return paperChooseBar();
    return undoEraseRedoBar();

  }

  void createPageBody(){
    PageBodyTest page = new PageBodyTest();
    pages.add(page);
  }
  Widget paperChooseBar(){
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child:
      Container(
          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50.0),
              color: Colors.greenAccent),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(child: Flexible(
                          child: FlatButton(
                              onPressed: () {
                                setState(() {
                                  pageBodyList[0].changePaperNumber(0);
                                });
                              },
                              padding: EdgeInsets.all(0.0),
                              child: Image.asset(
                                'assets/whitepapericon.png',
                                height: MediaQuery.of(context).size.width/6,
                                width:  MediaQuery.of(context).size.width/6,

                              )
                          ))),
                      Container(child: Flexible(
                          child: FlatButton(
                              onPressed:() {
                                setState(() {
                                  pageBodyList[0].changePaperNumber(1);
                                });
                              },
                              padding: EdgeInsets.all(0.0),
                              child: Image.asset(
                                'assets/legalpadicon.png',
                                height: MediaQuery.of(context).size.width/6,
                                width:  MediaQuery.of(context).size.width/6,

                              )
                          ))),
                      Container(child: Flexible(
                          child: FlatButton(
                              onPressed: () {
                                setState(() {
                                  pageBodyList[0].changePaperNumber(2);
                                });
                              },
                              padding: EdgeInsets.all(0.0),
                              child: Image.asset(
                                'assets/graphpaper.png',
                                height: MediaQuery.of(context).size.width/6,
                                width:  MediaQuery.of(context).size.width/6,

                              )
                          ))),

                      Container(child: Flexible(
                          child: FlatButton(
                              onPressed: () {
                                setState(() {
                                  pageBodyList[0].changePaperNumber(3);
                                });
                              },
                              padding: EdgeInsets.all(0.0),
                              child: Image.asset(
                                'assets/gridpapericon.png',
                                height: MediaQuery.of(context).size.width/6,
                                width:  MediaQuery.of(context).size.width/6,

                              )
                          ))),
                    ]
                ),
                Visibility(
                  child: (selectedMode == SelectedMode.Color)   //색 선택인 경우 getColorList()를 불러옴
                      ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: getColorList(),
                  )
                      : Slider(       // 색 선택이 아닌 경우 Slider를 불러옴 >
                      value: (selectedMode == SelectedMode.StrokeWidth)
                          ? strokeWidth
                          : opacity,
                      max: (selectedMode == SelectedMode.StrokeWidth)
                          ? 50.0
                          : 1.0,
                      min: 0.0,
                      onChanged: (val) {
                        setState(() {
                          if (selectedMode == SelectedMode.StrokeWidth)
                            strokeWidth = val;
                          else
                            opacity = val;
                        });
                      }),
                  visible: showBottomList,
                ),
              ],
            ),
          )
      ),
    );
  }
  void legalPadPressed(){
    setState(() {

    });
  }
//Widget scrollableButtons(){
//      List<Widget> widgets = [];
//      widgets.add(Padding(
//            padding: EdgeInsets.all(3.0),
//            child: Column(
//              children: <Widget>[
//                sizeButton(),
//              ],)));
//
//      widgets.add(Padding(
//          padding: EdgeInsets.all(3.0),
//          child: Column(
//            children: <Widget>[
//              cutButton(),
//            ],)));
//
//      widgets.add(Padding(
//          padding: EdgeInsets.all(3.0),
//          child: Column(
//            children: <Widget>[
//              eraserButton(),
//            ],)));
//
//      widgets.add(Padding(
//          padding: EdgeInsets.all(3.0),
//          child: Column(
//            children: <Widget>[
//              penButton(),
//            ],)));
//
//      widgets.add(Padding(
//          padding: EdgeInsets.all(3.0),
//          child: Column(
//            children: <Widget>[
//              fileButton(),
//            ],)));
//
//      widgets.add(Padding(
//          padding: EdgeInsets.all(3.0),
//          child: Column(
//            children: <Widget>[
//              colorButton(),
//            ],)));
//
//      widgets.add(Padding(
//          padding: EdgeInsets.all(3.0),
//          child: Column(
//            children: <Widget>[
//              refreshButton(),
//            ],)));
//
//      widgets.add(Padding(
//          padding: EdgeInsets.all(3.0),
//          child: Column(
//            children: <Widget>[
//              refreshButton(),
//            ],)));
//
//      widgets.add(Padding(
//          padding: EdgeInsets.all(3.0),
//          child: Column(
//            children: <Widget>[
//              refreshButton(),
//            ],)));
//
//
//      return ListView(
//          shrinkWrap: true,
//          scrollDirection: Axis.horizontal,
//          children: widgets,);
//}

  Widget pageBody(){
    return pageBodyList[0];
//    return pageBodyList[pageIndex];
  }

  // scaffold 관련 Widget
  Widget appBar(){
    return new AppBar(       // AppBar 부분 단순히 Text만 가짐
      backgroundColor: Colors.blueAccent,
      leading: new Container(),
      flexibleSpace: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          sizeButton(),
          cutButton(),
          penButton(),
          eraserButton(),
          colorButton(),
          fileButton(),
          refreshButton(),
          paperButton(),
          undoButton(),
          redoButton(),
        ],
      ),
    );
  }


  Widget colorChooseBar(){
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child:
      Container(
          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50.0),
              color: Colors.greenAccent),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,

              children: <Widget>[
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.album),
                          iconSize: 30,
                          onPressed: () {
                            setState(() {
                              if (selectedMode == SelectedMode.StrokeWidth)
                                showBottomList = !showBottomList;
                              selectedMode = SelectedMode.StrokeWidth;
                            });
                          }),
                      IconButton(
                          icon: Icon(Icons.opacity),
                          iconSize: 30,
                          onPressed: () {
                            setState(() {
                              if (selectedMode == SelectedMode.Opacity)
                                showBottomList = !showBottomList;
                              selectedMode = SelectedMode.Opacity;
                            });
                          }),
                      IconButton(
                          icon: Icon(Icons.color_lens),
                          iconSize: 30,
                          onPressed: () {
                            setState(() {
                              if (selectedMode == SelectedMode.Color)
                                showBottomList = !showBottomList;
                              selectedMode = SelectedMode.Color;
                            });
                          }),
                      IconButton(
                          icon: Icon(Icons.clear),
                          iconSize: 30,
                          onPressed: () {
                            setState(() {
                              showBottomList = false;
                              toggleColor();
                            });
                          }),
                    ]
                ),
                Visibility(
                  child: (selectedMode == SelectedMode.Color)   //색 선택인 경우 getColorList()를 불러옴
                      ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: getColorList(),
                  )
                      : Slider(       // 색 선택이 아닌 경우 Slider를 불러옴 >
                      value: (selectedMode == SelectedMode.StrokeWidth)
                          ? strokeWidth
                          : opacity,
                      max: (selectedMode == SelectedMode.StrokeWidth)
                          ? 50.0
                          : 1.0,
                      min: 0.0,
                      onChanged: (val) {
                        setState(() {
                          if (selectedMode == SelectedMode.StrokeWidth)
                            strokeWidth = val;
                          else
                            opacity = val;
                        });
                      }),
                  visible: showBottomList,
                ),
              ],
            ),
          )
      ),
    );
  }
  Widget undoEraseRedoBar(){
    return new PreferredSize(
        child: new ButtonBar(
          alignment:
          MainAxisAlignment.center,
          children: <Widget>[
            beforePageButton(),
            pageText(),
            nextPageButton(),
          ],


        ),preferredSize: Size.fromHeight(24.0)
    );
  }
  //color Choose Bar 버튼
  Widget beforePageButton(){
    return new ButtonTheme(
        child: new IconButton(
            icon:Icon(Icons.chevron_left,size: 30.0),
            onPressed: () {
              setState(() {
                pageBodyList[pageIndex].formerPage();
              });
            }
        )
    );
  }
  Widget nextPageButton(){
    return new ButtonTheme(
        child: new IconButton(
            icon:Icon(Icons.chevron_right,size: 30.0,),
            onPressed: () {
              setState(() {
                pageBodyList[pageIndex].nextPage();
              });
            }
        )
    );


  }
  Widget eraserButton(){
    return new ButtonTheme(
        child: new IconButton(
            icon:Icon(Icons.crop_square,size: 30.0,color: Colors.white,),
            onPressed: () {
              setState(() {
                pageBodyList[pageIndex].setEraser();
              });
            }
        )
    );
  }
  Widget pageText(){
    int pageNumber = pageBodyList[0].getPageNumber();
    return Text(
      '[$pageNumber/15]',
      textScaleFactor: 1,
      style: TextStyle(fontWeight: FontWeight.bold ,backgroundColor: Colors.greenAccent,color: Colors.black87,fontSize: 20),
    );

  }
  Widget redoButton(){
    return new ButtonTheme(
        child: new IconButton(
            icon:Icon(Icons.redo,size: 30.0),
            onPressed: () {
              setState(() {
                pageBodyList[pageIndex].setEraser();
              });
            }
        )
    );
  }
  Widget undoButton(){
    return new ButtonTheme(
        child: new IconButton(
            icon:Icon(Icons.undo,size: 30.0),
            onPressed: () {
              setState(() {
                pageBodyList[pageIndex].setEraser();
              });
            }
        )
    );
  }


  //AppBar Button
  Widget sizeButton(){
    return new ButtonTheme(
        child: new IconButton(
            icon:Icon(Icons.filter_center_focus,size: 30.0,color: Colors.white,),
            onPressed: () {
              setState(() {
                pageBodyList[pageIndex].setScaleMode();
              });
            }
        )
    );
  }
  Widget cutButton(){
    return new ButtonTheme(
        child: new IconButton(
            icon:Icon(Icons.content_cut,size: 30.0,color: Colors.white,),
            onPressed: () {
              setState(() {
                pageBodyList[pageIndex].setCutMode();
              });
            }
        )
    );
  }
  Widget penButton(){
    return new ButtonTheme(
      child: new IconButton(
          icon:Icon(Icons.mode_edit,size: 30.0,color: Colors.white,),
          onPressed: () {
            setState(() {
              pageBodyList[pageIndex].setPen();
            });
          }
      ),
    );
  }
  Widget fileButton(){
    return new ButtonTheme(
      child: new IconButton(
          icon:Icon(Icons.photo,size: 30.0,color: Colors.white,),
          onPressed: () {
            setState(() {
              pageBodyList[pageIndex].getImage();
            });
          }
      ),
    );
  }
  Widget colorButton(){
    return new ButtonTheme(
      child: new IconButton(
          icon:Icon(Icons.color_lens,size: 30.0,color: Colors.white,),
          onPressed: () {
            setState(() {
              toggleColor();
            });
          }
      ),
    );
  }
  Widget refreshButton(){
    return new ButtonTheme(
      child: new IconButton(
          icon:Icon(Icons.refresh,size: 30.0,color: Colors.white,),
          onPressed: () {
            setState(() {
              pageBodyList[0].refresh();
            });
          }
      ),
    );
  }
  Widget paperButton(){
    return new ButtonTheme(
      child: new IconButton(
          icon:Icon(Icons.wallpaper,size: 30.0,color: Colors.white,),
          onPressed: () {
            setState(() {
              togglePaper();
            });
          }
      ),
    );
  }

  void togglePaper(){
    paperChoose = paperChoose ? false : true;
  }

  void toggleColor(){
    colorChoose = colorChoose? false: true;
  }

  Widget colorCircle(Color color) {
    return GestureDetector(
      onTap: () {
        setState(() {
          pageBodyList[pageIndex].setColor(color);
        });
      },
      child: ClipOval(
        child: Container(
          padding: const EdgeInsets.only(bottom: 16.0),
          height: 36,
          width: 36,
          color: color,
        ),
      ),
    );
  }
  getColorList() {
    List<Widget> listWidget = List();
    for (Color color in colors) {
      listWidget.add(colorCircle(color));
    }
    Widget colorPicker = GestureDetector(             // 색깔 선택창에 대한 Widget
      onTap: () {
        showDialog(
          context: context,
          // ignore: deprecated_member_use
          child: AlertDialog(
            title: const Text('Pick a color!'),
            content: SingleChildScrollView(
              child: ColorPicker(
                pickerColor: pickerColor,
                onColorChanged: (color) {
                  pickerColor = color;
                },
                enableLabel: true,
                pickerAreaHeightPercent: 0.8,
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: const Text('Save'),
                onPressed: () {
                  setState(() => selectedColor = pickerColor);
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );
      },
      child: ClipOval(
        child: Container(
          padding: const EdgeInsets.only(bottom: 16.0),
          height: 36,
          width: 36,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.red, Colors.green, Colors.blue],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
              )),
        ),
      ),
    );
    listWidget.add(colorPicker);
    return listWidget;
  }

}


enum SelectedMode { StrokeWidth, Opacity, Color }
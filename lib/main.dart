import 'package:flutter/material.dart';
import 'paint.dart';
import 'list.dart';
import 'dart:developer';
import 'Fileio.dart';
import 'package:drawing_space/login.dart';
import'dart:convert';

import 'package:http/http.dart' as http;
import 'package:drawing_space/GridView.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:dio/dio.dart';
import 'package:drawing_space/http.dart';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:web_socket_channel/status.dart' as status;

import 'package:drawing_space/paint_test.dart';

//void main() => runApp(MyApp());

void main() async{
  await DotEnv().load('.env');
  final prefs = await SharedPreferences.getInstance();
  String _id;

  debugPrint("MAIN HERE");

  String _token = await (prefs.getString('ftoken') ?? null);
  _id = await (prefs.getString('fid') ?? 'no token');

  var token = _token;
  Dio dio = new Dio();
  var decoded;

  if(token == null){

    debugPrint("로그인 페이지ㄱㄱ");
    runApp(new MaterialApp(
        theme: new ThemeData(primarySwatch: Colors.orange),
        home: new SplashScreen(
            seconds: 2,
//        navigateAfterSeconds: new AfterSplash(res: widget.res),
            navigateAfterSeconds: new FacebookLoginPage(),
            title: new Text('LOADING...',
              style: new TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0
              ),),
            image: new Image.asset('assets/templogo1.png'),
            backgroundColor: Colors.white,
            styleTextUnderTheLoader: new TextStyle(),
            photoSize: 300,
            loaderColor: Colors.deepOrangeAccent
        )
    ));
  }
  else {
    var res;
    debugPrint('id: $_id');
    debugPrint('token: $token');
    dio.options.baseUrl = 'http://';
    dio.options.connectTimeout = 5000; //5s
    dio.options.receiveTimeout = 3000;
    try {
      res = await dio.get(
          'https://graph.facebook.com/v2.12/me?fields=name,email,permissions&access_token=$token');
      decoded = jsonDecode(res.data);
      debugPrint('res: ${decoded['name']},${decoded['email']}, token: $token');

//    return response;
    } catch (e) {
      runApp(new MaterialApp(
          theme: new ThemeData(primarySwatch: Colors.orange),
          home: new SplashScreen(
              seconds: 2,
//        navigateAfterSeconds: new AfterSplash(res: widget.res),
              navigateAfterSeconds: new FacebookLoginPage(),
              title: new Text('LOADING...',
                style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0
                ),),
              image: new Image.asset('assets/templogo1.png'),
              backgroundColor: Colors.white,
              styleTextUnderTheLoader: new TextStyle(),
              photoSize: 300,
              loaderColor: Colors.deepOrangeAccent
          )
//    home: new MyApp(res: _response), //FacebookLoginPage()
      ));
      debugPrint('res: FAILED');
      print(e);
//    return null;
    }

    if (res.statusCode == 200) {
      runApp(new MaterialApp(
          theme: new ThemeData(primarySwatch: Colors.orange),
          home: new SplashScreen(
              seconds: 2,
//        navigateAfterSeconds: new AfterSplash(res: widget.res),
              navigateAfterSeconds: new MyMainPage(id: _id, username: decoded['name']),
              title: new Text('LOADING...',
                style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0
                ),),
              image: new Image.asset('assets/templogo1.png'),
              backgroundColor: Colors.white,
              styleTextUnderTheLoader: new TextStyle(),
              photoSize: 300,
              loaderColor: Colors.deepOrangeAccent
          )
//    home: new MyApp(res: _response), //FacebookLoginPage()
      ));

      debugPrint("로그인 성공");
    } else {
      runApp(new MaterialApp(
          theme: new ThemeData(primarySwatch: Colors.orange),
          home: new SplashScreen(
              seconds: 2,
//        navigateAfterSeconds: new AfterSplash(res: widget.res),
              navigateAfterSeconds: new FacebookLoginPage(),
              title: new Text('LOADING...',
                style: new TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20.0
                ),),
              image: new Image.asset('assets/templogo1.png'),
              backgroundColor: Colors.white,
              styleTextUnderTheLoader: new TextStyle(),
              photoSize: 300,
              loaderColor: Colors.deepOrangeAccent
          )
//    home: new MyApp(res: _response), //FacebookLoginPage()
      ));
      debugPrint("로그인 실패");
//          showSnackBar(context, "로그인 실패");
    }
  }
//  runApp(new MaterialApp(
//    home: new FacebookLoginPage()
////    home: new MyApp(res: _response), //FacebookLoginPage()
//  ));
}

class MyApp extends StatefulWidget  {
  final Response res;
  final WebSocketChannel wschannel;

  MyApp({Key key, @required this.res, @required this.wschannel}): super(key:key);

  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
        seconds: 2,
//        navigateAfterSeconds: new AfterSplash(res: widget.res),
//        navigateAfterSeconds: MyMainPage(id: null, username:null,),
        title: new Text('Welcome In SplashScreen',
          style: new TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20.0
          ),),
        image: new Image.asset('pen.png'),
        backgroundColor: Colors.white,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 100.0,
        loaderColor: Colors.red
    );
  }
}


class MyMainPage extends StatefulWidget {
//  final Response res;
  final String id;
  String username;
  final WebSocketChannel wschannel = IOWebSocketChannel.connect(DotEnv().env['WS_IP_A']);

  void getUserInfo() async{
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    username = prefs.getString('fname');
    debugPrint("mainpage wsc: $wschannel");
  }

  Response response;
  MyMainPage({Key key, @required this.id, this.username}): super(key:key);


  @override
  _MyMainPageState createState() => _MyMainPageState();
}

class _MyMainPageState extends State<MyMainPage> {    // 이 클래스가 만들어질때 build는 Scafold를 만들어서 return
  List<Offset> points = <Offset>[];//Offset Class의 객체를 요소로 가지는 List의 이름 points.
  Response _res;
  String _id;
  String username;



  void GetChannelListByID(String id) async {
    MyHttpRequests http = MyHttpRequests();
    Response _response;
    _response = await http.makeHttpRequest('showchannel/$id').then((_response){
      setState(() {
        this._res = _response;
      });
      return null;
    });
    debugPrint('channellist: ${_response}');
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _id = widget.id;
    GetChannelListByID(_id);
    widget.getUserInfo();

    try {
      widget.wschannel.stream.listen((data) {

        }
      );

      debugPrint('wschannel is ${widget.wschannel}');
    }catch(e){

      debugPrint("Already listening");
    }
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: false,
      drawer: drawer(),
      appBar: AppBar(       // AppBar 부분 단순히 Text만 가짐
          title: Container(padding: EdgeInsets.all(18),child:Image.asset('assets/templogo2.png'),),
          iconTheme: new IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,

          centerTitle: true,

      ),

      body:Appbody(),

    );

  }

  Widget Appbody(){
    if(_res==null){
      return new Scaffold(
          body:Row(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment:MainAxisAlignment.center, children:<Widget>[Column(mainAxisAlignment:MainAxisAlignment.center,children:<Widget>[CircularProgressIndicator()])]));
    }else{
      return TodoApp(wschannel: widget.wschannel, res: _res, userid: _id, username: widget.username);
    }
  }

//  @override
  Widget drawer() {
    return new Drawer(

//      padding: EdgeInsets.fromLTRB(0, 0, 0, 100),
      child: new ListView(
        shrinkWrap: true,
        children: <Widget>[
          new UserAccountsDrawerHeader(
            accountName: Text(widget.username, style: TextStyle(fontSize: 18, color: Colors.white, fontWeight: FontWeight.bold)),
            accountEmail: Text(widget.id, style: TextStyle(color: Colors.white)),
            currentAccountPicture: new CircleAvatar(backgroundColor: Colors.white, child: new Text(widget.username[widget.username.length-1], style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)))
          ),

//          new ListTile(
//            title: new Text('Edit Profile'),
//            trailing: new Icon(Icons.keyboard_arrow_right),
//          ),
          new ListTile(
            title: new Text('Log Out'),
            trailing: new Icon(Icons.keyboard_arrow_right),
            onTap: () async {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              widget.wschannel.sink.close();
              prefs.clear();
              Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) =>
                      FacebookLoginPageApp()
                  )
              );
            }
          ),
          new Divider(),
          new ListTile(
            title: new Text('Close'),
            trailing: new Icon(Icons.keyboard_arrow_right),
            onTap: ()=>Navigator.of(context).pop()
          ),
          new Divider(),


        ],

      )
    );

  }
}

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'Car', icon: Icons.directions_car),
  const Choice(title: 'Bicycle', icon: Icons.directions_bike),

];




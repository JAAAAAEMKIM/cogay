////다음에 해보겠습니다.
//
//
//import 'dart:async';
//import 'dart:io';
//import 'dart:math';
//import 'dart:ui';
//import 'dart:convert';
//import 'dart:collection';
//import 'package:flutter/material.dart';
//import 'package:flutter_colorpicker/flutter_colorpicker.dart';
//import 'package:drawing_space/image_picker_handler.dart';
//import 'package:drawing_space/image_picker_dialog.dart';
//import 'package:web_socket_channel/io.dart';
//import 'package:web_socket_channel/web_socket_channel.dart';
//import 'package:web_socket_channel/status.dart' as status;
//
//
//class WebSocketFunctions{
//  final WebSocketChannel channel = IOWebSocketChannel.connect('ws://192.168.0.6:8080');
//  Color selectedColor = Colors.black;
//  Color pickerColor = Colors.black;
//  List<DrawingPoints> points = List();
//  bool showBottomList = false;
//  bool changeEraser = false;
//  bool showBottomNavigationBar = false;
//  bool scaleMotion = false;
//  bool colorChoose = false;
//  bool cropping = false;
//  double opacity = 1.0;
//  StrokeCap strokeCap = StrokeCap.round;
//
//
//  Queue pointListStack = new Queue();
//  Queue redoStack = new Queue();
//  List<Color> ButtonColors = [
//    Colors.white,
//    Colors.white,
//    Colors.white,
//    Colors.white,
//    Colors.white,
//    Colors.white,
//    Colors.white
//  ];
//  int current_index = 0;
//  int new_index;
//  double move_x = 0;
//  double move_y = 0;
//  double start_x;
//  double start_y;
//  File _image = null;
//  ImagePickerHandler imagePicker;
//  AnimationController _controller;
//
//
//  @override
//  void initState() {
//    channel.stream.listen((data) {
//      decode(data);
//    });
//  }
//
//  // Server 관련
//  String encode(List<DrawingPoints> data) {
//    String result = '';
//    int a = data.length;
//
//    for (int i = 0; i < a; i++) {
//      result += (data[i].toString() + '|');
//    }
//    return result;
//  }
//  void decode(String data) {
//    List<String> datalist = data.split("|");
//    int length = datalist.length;
//    points.clear();
//    for (int i = 0; i < length - 1; i++) {
//      if (datalist[i] == "null") {
//        points.add(null);
//        continue;
//      }
//      else {
//        List<String> datalist2 = datalist[i].split("*");
//        int color = int.parse(datalist2[0].substring(6,16));
//        double strokeWidth = double.parse(datalist2[1]);
//        List<String> datalist3 = datalist2[2].split(",");
//        double dx = double.parse(datalist3[0].substring(7));
//        double dy = double.parse(datalist3[1].substring(1,5));
//        double size;
//        if (datalist2[3] == "null") {
//          size = null;
//        }
//        else {
//          size = double.parse(datalist2[3]);
//        }
//        RenderBox renderBox = findRenderObject();
//
//        points.add(DrawingPoints(
//          points: renderBox.globalToLocal(Offset(dx, dy)),
//          paint: Paint()
//            ..strokeCap = strokeCap
//            ..isAntiAlias = true
//            ..color = Color(color)
//            ..strokeWidth = strokeWidth,
//          size: size,
//        ));
//      }
//    }
//  }
//  void _sendData() {
//    channel.sink.add(
//        jsonEncode({
//          'event': "draw",
//          'data': encode(points),
//        })
//    );
//  }
//}
//
//class DrawingPoints {
//  Paint paint;
//  Offset points;
//  double size;
//
//  DrawingPoints({this.points, this.paint, this.size});
//  String toString() => paint.color.toString() + "*" + paint.strokeWidth.toString() + "*" + points.toString() + "*" +size.toString();
//}
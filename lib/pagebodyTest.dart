import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'dart:ui';
import 'dart:convert';
import 'dart:collection';
import 'package:drawing_space/paint.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:photo_view/photo_view.dart';
import 'package:drawing_space/image_picker_handler.dart';
import 'package:drawing_space/image_picker_dialog.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:web_socket_channel/status.dart' as status;
import 'package:path_drawing/path_drawing.dart';
import 'package:photo_view/photo_view.dart';
//import 'package:drawing_space/picture_controller.dart';
import 'package:drawing_space/PageBody.dart';
import 'package:drawing_space/GridView.dart';
import 'package:drawing_space/http.dart';
import 'package:drawing_space/Fileio.dart';
import 'package:dio/dio.dart';

import 'package:back_button_interceptor/back_button_interceptor.dart';

class PageBodyTest extends StatefulWidget {

  PageBodyTest({Key key})
      : super(key: key);


  _PageBodyTest pageBody = new _PageBodyTest();
  @override

  _PageBodyTest createState() => pageBody ;

  bool eraserMode(){
    return pageBody.eraserMode;
  }
  bool penMode(){
    return pageBody.penMode;
  }
  bool scaleMode(){
    return pageBody.scaleMode;
  }
  bool cutMode(){
    return pageBody.cutMode;
  }

  void clearRectanglePoints()
  {
    pageBody.rectanglePoints.clear();
  }

  void movePointsClear(){
    pageBody.movePoints.clear();
  }

  void rectangleChosenFalse(){
    pageBody.rectangleChosenFalse();
  }

  void pointsClear(){
    pageBody.points.clear();
  }

  void changePaperNumber(int number){
    pageBody.paperNumber = number;
    print(pageBody.paperNumber);
    pageBody.pageSetState();
    pageBody.pagePaper[pageBody.pageIndex] = number;
  }

  List<DrawingPoints> getPoints(){
    return pageBody.points;
  }
  void pageSetState(){
    pageBody.pageSetState();
  }

  void pointListStackAdd(List<DrawingPoints> points){
    pageBody.pointListStack.add(points);
  }

  List<DrawingPoints> pointListStackRemove(){
    return pageBody.pointListStack.removeLast();
  }
  void redoStackAdd(List<DrawingPoints> points){
    pageBody.redoStack.add(points);
  }

  List<DrawingPoints> redoStackRemove(){
    return pageBody.redoStack.removeLast();
  }

  void copyList(List<DrawingPoints> point,List<DrawingPoints> tempList){
    pageBody.copyList(point, tempList);
  }

  void redo(){
    pageBody.redo();
  }

  void nextPage(){
    pageBody.nextPage();
  }

  void formerPage(){
    pageBody.formerPage();
  }

  void undo(){
    pageBody.undo();
  }

  void refresh(){
    pageBody.refresh();
  }
  void setColor(Color color){
    pageBody.setColor(color);
  }

  Queue getPointListStack(){
    return pageBody.pointListStack;
  }

  int getPointListStackLength(){
    return pageBody.pointListStack.length;
  }

  int getPageNumber(){
    return pageBody.pageIndex+1;
  }

  void setEraser(){
    pageBody.setEraser();
    pageBody.pageSetState();
  }

  void setPen(){
    pageBody.setPen();
    pageBody.pageSetState();
  }
  void getImage(){
    pageBody.getImage();
  }

  void setScaleMode(){
    pageBody.setScaleMode();
    pageBody.pageSetState();
  }
  void setCutMode(){
    pageBody.setCutMode();
    pageBody.pageSetState();
  }

  void changeButtonColor(){
    pageBody.changeButtonColor();
  }
  void toggleSize(){
    pageBody.toggleSize();
  }

  void setSelectedMode(SelectedMode selectedMode){
    pageBody.selectedMode = selectedMode;
  }
}

class _PageBodyTest extends State<PageBodyTest> with TickerProviderStateMixin,ImagePickerListener{
  Color selectedColor = Colors.black;
  List<DrawingPoints> points = List();
  List<Offset> copiedPoints= List();
  List<List<DrawingPoints>> undoTemp = [List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List()];
  List<Color> colors = [Colors.red, Colors.green, Colors.blue, Colors.amber, Colors.black];
  List<Color> buttonColors = [Colors.white,Colors.white,Colors.white,Colors.white, Colors.white, Colors.white,Colors.white];
  List<Offset> rectanglePoints = List();
  List<Offset> movePoints = List();
  List<List<DrawingPoints>> pagePoints = [List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List()]; //page 15개
  List<List<DrawingPoints>> otherPagePoints = [List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List()];
  int pageIndex = 0 ;


  List<int> pagePaper = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
  List<ExactAssetImage> pageImage = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,];


  double opacity = 1.0;
  double strokeWidth = 3.0;
  double scaleHeight = 0;
  StrokeCap strokeCap = StrokeCap.round;
  SelectedMode selectedMode = SelectedMode.StrokeWidth;
  Queue pointListStack = new Queue();
  Queue redoStack = new Queue();

  Queue pageNumberUndoStack = new Queue();
  Queue pageNumberRedoStack = new Queue();

  int currentIndex = 0;
  int newIndex;
  int paperNumber = 0;


  File image = null;
  List<File> images = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,];
  bool showBottomList = false;
  bool changeEraser = false;
  bool showBottomNavigationBar = false;
  bool colorChoose = false;
  bool cropping = false;
  bool rectangleChosen = false;
  bool changeSize = false;



  bool eraserMode = false;
  bool penMode = true;
  bool scaleMode = false;
  bool cutMode = false;




  IOWebSocketChannel wschannel;
  ImagePickerHandler imagePicker;
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );

    imagePicker=new ImagePickerHandler(this,_controller);
    imagePicker.init();
    debugPrint('first');
  }

  @override
  Widget build(BuildContext context) {
    return body();
  }

  Widget body(){
    return new Stack(alignment: const Alignment(0.0, 0.0),
      children: <Widget>[
        photoView(),
        chooseGesture(),
      ],
    );
  }

  Widget photoView(){
    return new Container(
        child:
        PhotoView.customChild(//PhotoView 안에있는 객체들은 모두 고정된 애들. No Gesture.
          child:
          Stack(alignment: const Alignment(0.0, 0.0),
            children: <Widget>[
              backPaper(),
              (images[pageIndex] == null)? block(): backImage(),
              drawingCanvas(),
              rectangleDrawingCanvas(),
            ],
          ),
          childSize: Size(MediaQuery.of(context).size.width, MediaQuery.of(context).size.height-AppBar().preferredSize.height*1.5),
          initialScale: 1.0,
          minScale: 1.0,
          maxScale: 5.0,
          backgroundDecoration:
          new BoxDecoration(
            color: Colors.white,
          ),
        )
    );

  }
  //photoView 구성위젯
  Widget backPaper(){
    if(paperNumber == 0 ){
      return new Image(
          height: MediaQuery.of(context).size.height-AppBar().preferredSize.height*1.5,
          width: MediaQuery.of(context).size.width,
          image: AssetImage("assets/whitepaper.png")
      );
    }
    else if(paperNumber == 1){
      return new Image(
          height: MediaQuery.of(context).size.height-AppBar().preferredSize.height*1.5,
          width: MediaQuery.of(context).size.width,
          image: AssetImage("assets/legalpad2.png")
      );
    }
    else if(paperNumber == 2){
      return new Image(
          height: MediaQuery.of(context).size.height-AppBar().preferredSize.height*1.5,
          width: MediaQuery.of(context).size.width,
          image: AssetImage("assets/graphpaper.png")
      );
    }
    else if(paperNumber == 3){
      return new Image(
          height: MediaQuery.of(context).size.height-AppBar().preferredSize.height*1.5,
          width: MediaQuery.of(context).size.width,
          image: AssetImage(
              "assets/gridpaper2.png"
          )
      );
    }

  }
  Widget backImage(){
    ExactAssetImage tempImage =new ExactAssetImage(images[pageIndex].path); // image도 계속초기화를 해야함.
    pageImage[pageIndex] = tempImage;
    print(pageIndex);
    print(pageImage[pageIndex]);
    return new Image(
      height: MediaQuery.of(context).size.height*0.8,
      width: MediaQuery.of(context).size.width*0.8,
      image : pageImage[pageIndex],
      //      alignment: new Alignment(-0.05, moveY),  사진을 옮기는 라인.
    );


  }
  Widget otherDrawingCanvas(){
    return Container(
        padding: const EdgeInsets.all(10.0),
        child: CustomPaint(
          size:  Size(MediaQuery.of(context).size.width, MediaQuery.of(context).size.height),
          painter: DrawingPainter(
            pointsList: otherPagePoints[pageIndex],
          ),
        )
    );
  }
  Widget drawingCanvas(){
    return Container(
        padding: const EdgeInsets.all(10.0),
        child: CustomPaint(
          size:  Size(MediaQuery.of(context).size.width, MediaQuery.of(context).size.height),
          painter: DrawingPainter(
            pointsList: points,
          ),
        )
    );
  }
  Widget rectangleDrawingCanvas(){
    return CustomPaint(
      size: Size.infinite,
      painter: RectanglePainter(rectanglePoints: rectanglePoints, movePoints: movePoints),
    );
  }
  Widget chooseGesture(){


    if(penMode == true) return pen();
    if(eraserMode == true) return eraser();
    if(cutMode == true) return cut();
    if(scaleMode == true) return block();
    return block();
  }
  //chooseGesture 구성위젯
  Widget pen(){
    return new GestureDetector(
      onScaleStart: (scaleUpdates){
        // scaleMode를 써야 한 점씩만 인식.
        setState(() {
          points.add(DrawingPoints(
            points: Offset(scaleUpdates.focalPoint.dx,scaleUpdates.focalPoint.dy-AppBar().preferredSize.height*1.5),
            paint: Paint()
              ..strokeCap = strokeCap
              ..isAntiAlias = true
              ..color = selectedColor.withOpacity(opacity)
              ..strokeWidth = strokeWidth,
            size: strokeWidth,
          ));
        });

      },
      onScaleUpdate: (scaleUpdates){
        if( scaleMode) return;
        if (scaleUpdates.scale == 1.0) {  // scaleMode를 써야 한 점씩만 인식.
          setState(() {
            points.add(DrawingPoints(
              points: Offset(scaleUpdates.focalPoint.dx,scaleUpdates.focalPoint.dy-AppBar().preferredSize.height*1.5),
              paint: Paint()
                ..strokeCap = strokeCap
                ..isAntiAlias = true
                ..color = selectedColor.withOpacity(opacity)
                ..strokeWidth = strokeWidth,
              size: strokeWidth,
            ));
          });
        }
      },
      onScaleEnd: (scaleEndDetails) {
        if( scaleMode) return;
        setState(() {
          points.add(null);
          copyList(points, undoTemp[pointListStack.length]);
          pointListStack.addLast(undoTemp[pointListStack.length]);
          redoStack.clear();
          pageNumberUndoStack.addLast(pageIndex);
          pageNumberRedoStack.clear();


        });
      },

    );
  }
  Widget eraser(){
    return new GestureDetector(   // changeEraser이 true 인 경우 > pointslist를 제거함.
      onPanUpdate: (details) {
        setState(() {
          RenderBox renderBox = context.findRenderObject();
          Offset offset = Offset(details.globalPosition.dx,details.globalPosition.dy-(AppBar().preferredSize.height));

          for(int i = 0; i< points.length; i++){
            if (points[i] == null){
              continue;
            }
            double dx = points[i].points.dx - offset.dx;
            double dy = points[i].points.dy - offset.dy;
            double distance = sqrt(dx*dx +dy*dy);
            if(distance < 20 ){
              points[i] = null;
            }
          }
        });
      },
      onPanStart: (details) {
        setState(() {
          Offset offset = Offset(details.localPosition.dx,details.localPosition.dy-(AppBar().preferredSize.height));

          for(int i = 0; i< points.length; i++){
            if (points[i] == null){
              continue;
            }
            double dx = points[i].points.dx - offset.dx;
            double dy = points[i].points.dy - offset.dy;
            double distance = sqrt(dx*dx +dy*dy);
            if(distance < 20 ){
              points[i] = null;
            }

          }
        });
      },

      onPanEnd: (details) {
        copyList(points, undoTemp[pointListStack.length]);
        pointListStack.addLast(undoTemp[pointListStack.length]);
        pageNumberUndoStack.addLast(pageIndex);
        redoStack.clear();
        pageNumberRedoStack.clear();

        setState(() {
        });
      },
    );
  }
  Widget cut(){
    if(rectangleChosen == false) {
      double startX;
      double startY;
      double endX;
      double endY;
      return new GestureDetector(
        onPanStart: (details) {
          setState((){
            rectanglePoints.clear();
            movePoints.clear(); // check
            startX = details.globalPosition.dx;
            startY = details.globalPosition.dy;
            rectanglePoints.add(new Offset(startX, startY).translate(
                0.0, -(AppBar().preferredSize.height) + scaleHeight));
          });
        },
        onPanUpdate:(details) {
          setState(() {
            endX = details.globalPosition.dx;
            endY = details.globalPosition.dy;
            rectanglePoints.add(new Offset(endX, endY).translate(
                0.0, -(AppBar().preferredSize.height) + scaleHeight));
          });
        },
        onPanEnd:(details) {
          rectangleChosen = true;
          addPoints();  // 사각형이 정해졌다면 점들을 추가.
        },
      );
    }
    else{
      double startX;
      double startY;
      double endX;
      double endY;
      double x1 = rectanglePoints[0].dx;
      double x2 = rectanglePoints[rectanglePoints.length-1].dx;
      double y1 = rectanglePoints[0].dy;
      double y2 = rectanglePoints[rectanglePoints.length-1].dy;

      if(x1 < x2 && y1 >y2 ){
        double temp = y2;
        y2 = y1;
        y1 = temp;
      }

      if(x1 > x2 && y2 >y1 ){
        double temp = x2;
        x2 = x1;
        x1 = temp;
      }

      if(x1 > x2 && y1 >y2 ){
        double temp = y2;
        y2 = y1;
        y1 = temp;

        temp = x2;
        x2 = x1;
        x1= temp;
      }
      return new GestureDetector(
        onPanStart: (details) {    // 시작 점을 기억.
          setState(() {

            startX = details.globalPosition.dx;
            startY = details.globalPosition.dy;
            if(startX > (x1) && startX < (x2) && startY -(AppBar().preferredSize.height) + scaleHeight> (y1) && startY-(AppBar().preferredSize.height) + scaleHeight < (y2)){      // 안쪽을 누른 경우 > 이동 가능
              copiedPoints.clear();
              movePoints.clear();
              movePoints.add(new Offset(startX, startY));

              for (int i = 0; i < points.length; i++) {
                if (points[i] == null){
                  copiedPoints.add(null);
                  continue;
                }
                Offset temp = new Offset(points[i].points.dx, points[i].points.dy);
                copiedPoints.add(temp);
              }
            }
            else{
              rectanglePoints.clear();        // 사각형을 안보이게
              movePoints.clear();            // pan의 첫점 끝 점 차이를 저장한 거 clear
              copiedPoints.clear();
              rectangleChosen = false;

            }
          });
        },
        onPanUpdate:(details) {
          double dx = 0;
          double dy = 0;
          if(cutMode == false) return;
          setState(() {
            endX = details.globalPosition.dx;
            endY = details.globalPosition.dy;
            if(movePoints != null&& movePoints.length !=0) {
              dx = endX - movePoints[0].dx;
              dy = endY - movePoints[0].dy;
              movePoints.add(new Offset(dx,dy));
              updatePoints(x1,x2,y1,y2,dx,dy);
            }
          });
        },
        onPanEnd: (details){


          copyList(points, undoTemp[pointListStack.length]);
          pointListStack.addLast(undoTemp[pointListStack.length]);
          redoStack.clear();
          pageNumberUndoStack.addLast(pageIndex);
          pageNumberRedoStack.clear();

          setState(() {
            rectanglePoints.clear();        // 사각형을 안보이게
            movePoints.clear();            // pan의 첫점 끝 점 차이를 저장한 거 clear
            copiedPoints.clear();
            rectangleChosen = false;
            cutMode = false;
          });

        },
      );
    }
  }
  Widget block(){
    return new Container(
      color: Colors.black,
      height: 0,
      width: MediaQuery.of(context).size.width,
    );

  }


  void setPen(){
    eraserMode = false;
    penMode = true;
    scaleMode = false;
    cutMode = false;
  }
  void setEraser() {
    eraserMode = true;
    penMode = false;
    scaleMode = false;
    cutMode = false;
  }
  void setScaleMode(){
    penMode = false;
    eraserMode = false;
    scaleMode = true;
    cutMode = false;
  }
  void setCutMode(){
    penMode = false;
    eraserMode = false;
    scaleMode = false;
    cutMode = true;
  }

  void changeButtonColor(){
    buttonColors[currentIndex] = Colors.white;
    buttonColors[newIndex] = Colors.grey;
    currentIndex = newIndex;
  }
  void toggleSize(){
    changeSize = changeSize? false: true;
  }

  copyList(List<DrawingPoints> point, List<DrawingPoints> templist){

    if(templist.length != 0) templist.clear();
    for(int i = 0 ; i < point.length; i++){
      if(point[i] == null){
        templist.add(null);
        continue;
      }
      templist.add(DrawingPoints(points: Offset(point[i].points.dx,point[i].points.dy),paint: point[i].paint,size:point[i].size));
    }
  }
  void addPoints(){
    for( int i = 0 ; i < points.length-1 ; i++){
      if(points[i] == null || points[i+1] == null) continue;

      double x1 = points[i].points.dx ;
      double y1 = points[i].points.dy;
      double x2 = points[i+1].points.dx;
      double y2 = points[i+1].points.dy;

      double deltaY = y2-y1;
      double deltaX = x2-x1;
      double tangent = deltaY/deltaX;
      double invTangent = 1/tangent;

      double xF = rectanglePoints[0].dx;
      double yF = rectanglePoints[0].dy;
      double xL = rectanglePoints[rectanglePoints.length-1].dx;
      double yL = rectanglePoints[rectanglePoints.length-1].dy;

      if(xF < xL && yF >yL ){
        double temp = yL;
        yL = yF;
        yF = temp;
      }

      if(xF > xL && yL >yF ){
        double temp = xF;
        xF = xL;
        xL = temp;
      }

      if(xF > xL && yF >yL ){
        double temp = yL;
        yL = yF;
        yF = temp;

        temp = xL;
        xL = xF;
        xF= temp;
      }


      if(x1<xF && (x2>xF && x2<xL && y2>yF && y2<yL)) { // XF에 대한 경계 일때  out> in

        double xl = xF - 0.0001;
        double yl = y1 + ((xF - 0.0001)-x1)*tangent ;
        double xr = xF + 0.0001;
        double yr = y2 - (x2- (xF + 0.0001))*tangent ;
        points.insert(i + 1, new DrawingPoints(points: Offset(xl, yl),
            paint: points[i].paint,
            size: points[i].size));
        points.insert(i + 2, new DrawingPoints(points: Offset(xr, yr),
            paint: points[i].paint,
            size: points[i].size));
        points.insert(i+2 , null);
        continue;
      }

      if(x2<xF && (x1>xF && x1<xL && y1>yF && y1<yL)) { // XF에 대한 경계 일때  in> out
        double xr = xF + 0.0001;
        double yr = y1 - (x1-(xF+0.0001))*tangent ;
        double xl = xF - 0.0001;
        double yl = y2 + ( (xF - 0.0001)-x2)*tangent ;
        points.insert(i + 1, new DrawingPoints(points: Offset(xr, yr),
            paint: points[i].paint,
            size: points[i].size));
        points.insert(i + 2, new DrawingPoints(points: Offset(xl, yl),
            paint: points[i].paint,
            size: points[i].size));
        points.insert(i+2 , null);
        continue;
      }

      if(y1<yF && (x2>xF && x2<xL && y2>yF && y2<yL)) { // YF 가 경계일 떄  out > in
        double yt = yF - 0.0001;
        double xt = x1 + ((yF-0.0001) -y1)*invTangent;
        double yb = yF + 0.001;
        double xb = x2 - ( y2 - (yF+0.0001))*invTangent;
        points.insert(i + 1, new DrawingPoints(points: Offset(xt, yt),
            paint: points[i].paint,
            size: points[i].size));
        points.insert(i + 2, new DrawingPoints(points: Offset(xb, yb),
            paint: points[i].paint,
            size: points[i].size));
        points.insert(i+2 , null);
        continue;
      }

      if(y2<yF && (x1>xF && x1<xL && y1>yF && y1<yL)) { // YF 가 경계일 떄  in > out
        double yt = yF - 0.0001;
        double xt = x2 + ((yF - 0.0001) -y2)*invTangent;
        double yb = yF + 0.0001;
        double xb = x1 - ( y1 - (yF+0.0001))*invTangent;
        points.insert(i + 1, new DrawingPoints(points: Offset(xb, yb),
            paint: points[i].paint,
            size: points[i].size));
        points.insert(i + 2, new DrawingPoints(points: Offset(xt, yt),
            paint: points[i].paint,
            size: points[i].size));
        points.insert(i+2 , null);
        continue;
      }


      if(x1>xL && (x2>xF && x2<xL && y2>yF && y2<yL)) { // XL에 대한 경계 일때  out > in
        double xr = xL + 0.0001;
        double yr = y1 - (x1 - (xL + 0.0001))*tangent ;
        double xl = xL - 0.0001;
        double yl = y2 + ( (xL - 0.0001)-x2)*tangent ;
        points.insert(i + 1, new DrawingPoints(points: Offset(xr, yr),
            paint: points[i].paint,
            size: points[i].size));
        points.insert(i + 2, new DrawingPoints(points: Offset(xl, yl),
            paint: points[i].paint,
            size: points[i].size));
        points.insert(i+2 , null);
        continue;
      }

      if(x2>xL && (x1>xF && x1<xL && y1>yF && y1<yL)) { // XL에 대한 경계 일때  in > out
        double xr = xL + 0.0001;
        double yr = y2 - (x2 - (xL + 0.0001))*tangent ;
        double xl = xL - 0.0001;
        double yl = y1 + ( (xL - 0.0001)-x1)*tangent ;
        points.insert(i + 1, new DrawingPoints(points: Offset(xl, yl),
            paint: points[i].paint,
            size: points[i].size));
        points.insert(i + 2, new DrawingPoints(points: Offset(xr, yr),
            paint: points[i].paint,
            size: points[i].size));
        points.insert(i+2 , null);
        continue;
      }

      if(y1>yL && (x2>xF && x2<xL && y2>yF && y2<yL)) { // YL 가 경계일 떄  out > in
        double yb = yL + 0.0001;
        double xb = x1 - (y1 - (yL+0.0001) )*invTangent;
        double yt = yL - 0.001;
        double xt = x2 + ( (yL-0.0001) - y2 )*invTangent;
        points.insert(i + 1, new DrawingPoints(points: Offset(xb, yb),
            paint: points[i].paint,
            size: points[i].size));
        points.insert(i + 2, new DrawingPoints(points: Offset(xt, yt),
            paint: points[i].paint,
            size: points[i].size));
        points.insert(i+2 , null);
        continue;
      }

      if(y2>yL && (x1>xF && x1<xL && y1>yF && y1<yL)) { // YL 가 경계일 떄  in > out
        double yb = yL + 0.0001;
        double xb = x2 - (y2 - (yL+0.0001) )*invTangent;
        double yt = yL - 0.0001;
        double xt = x1 + ( (yL-0.0001) - y1 )*invTangent;
        points.insert(i + 1, new DrawingPoints(points: Offset(xt, yt),
            paint: points[i].paint,
            size: points[i].size));
        points.insert(i + 2, new DrawingPoints(points: Offset(xb, yb),
            paint: points[i].paint,
            size: points[i].size));
        points.insert(i+2 , null);
        continue;
      }

    }


  }
  void updatePoints(double x1, double x2, double y1, double y2, double dx, double dy){

    for( int i = 0 ; i < copiedPoints.length ; i++){
      if(copiedPoints[i] == null) continue;
//       if(i == 0){print(copiedPoints[0]);}
      double x = copiedPoints[i].dx;
      double y = copiedPoints[i].dy;
      if(x<x1) continue;
      if(x>x2) continue;
      if(y<y1) continue;
      if(y>y2) continue;
//       if( j == 0){ points[i] = null; copiedPoints[i] = null; j++;}
      points[i].points = new Offset(x+dx,y+dy);
    }


  }


  void redo(){
    if(redoStack.length == 0){
    }
    else {
      List<DrawingPoints> temp = redoStack.removeLast();
      copyList( temp, points);
      pointListStack.addLast(temp);
      int tempPageNumber = pageNumberRedoStack.removeLast();
      pageIndex = tempPageNumber;
      pageNumberUndoStack.addLast(pageIndex);
    }

  }
  void pageSetState(){
    setState(() {
    });
  }
  void undo(){
    print(pointListStack.length);
    if(pointListStack.length == 1){
      List<DrawingPoints> tempRedo = pointListStack.removeLast();

      redoStack.addLast(tempRedo);
      points.clear();
    }
    else if(pointListStack.length == 0){
    }
    else {
      List<DrawingPoints> tempRedo = pointListStack.removeLast();
      redoStack.addLast(tempRedo);

      int temp1 = pageNumberUndoStack.removeLast();
      pageNumberRedoStack.addLast(temp1);


      List<DrawingPoints> temp = pointListStack.removeLast();
      copyList(temp, points); // 맨뒤에서 두번째
      pointListStack.addLast(temp);

      int temp2 = pageNumberUndoStack.removeLast();
      pageIndex = temp2;
      pageNumberUndoStack.addLast(temp2);
    }
  }
  void refresh(){
    newIndex = 4;
    changeButtonColor();
    points.clear();
    pointListStack.add(points);


  }

  void nextPage(){
    if(pageIndex == 14) {
      print("cannot create anymore page");
      return;
    }
    copyList( points, pagePoints[pageIndex]);  // 현재 page의 points를 저장.s
    setState(() {
      pageIndex++;
    });
    copyList(pagePoints[pageIndex],points);   // 다음 page의 points를 불러옴

    // Queue에 저장함.
    copyList(points, undoTemp[pointListStack.length]);
    pointListStack.addLast(undoTemp[pointListStack.length]);
    redoStack.clear();
    pageNumberUndoStack.addLast(pageIndex);
    pageNumberRedoStack.clear();

    setState(() {
      paperNumber = pagePaper[pageIndex];
    });
  }
  void formerPage(){
    if(pageIndex == 0 ) return;

    copyList( points, pagePoints[pageIndex]); // 현재 page의 points를 저장.
    pageIndex--;
    copyList(pagePoints[pageIndex],points); // 이전 page의 points를 저장.

    // Queue에 저장함.
    copyList(points, undoTemp[pointListStack.length]);
    pointListStack.addLast(undoTemp[pointListStack.length]);
    redoStack.clear();
    pageNumberUndoStack.addLast(pageIndex);
    pageNumberRedoStack.clear();
    setState(() {
      paperNumber = pagePaper[pageIndex];
    });

  }

  void rectangleChosenFalse(){
    rectangleChosen = false;
  }

  void getImage(){
    imagePicker.showDialog(context);
  }
  void setColor(Color color){
    selectedColor = color;
  }


  String encode(List<DrawingPoints> data) {
    String result = '';
    int a = data.length;

    for (int i = 0; i < a; i++) {
      result += (data[i].toString() + '|');
    }
    return result;
  }
  void decode(String data) {
    List<String> datalist = data.split("|");
    int length = datalist.length;
    points.clear();
    for (int i = 0; i < length - 1; i++) {
      if (datalist[i] == "null") {
        points.add(null);
        continue;
      }
      else {
        List<String> datalist2 = datalist[i].split("*");
        int color = int.parse(datalist2[0].substring(6,16));
        double strokeWidth = double.parse(datalist2[1]);
        List<String> datalist3 = datalist2[2].split(",");
        double dx = double.parse(datalist3[0].substring(7));
        double dy = double.parse(datalist3[1].substring(1,5));
        double size;
        if (datalist2[3] == "null") {
          size = null;
        }
        else {
          size = double.parse(datalist2[3]);
        }
        RenderBox renderBox = context.findRenderObject();

        points.add(DrawingPoints(
          points: renderBox.globalToLocal(Offset(dx, dy)),
          paint: Paint()
            ..strokeCap = strokeCap
            ..isAntiAlias = true
            ..color = Color(color)
            ..strokeWidth = strokeWidth,
          size: size,
        ));
      }
    }
  }

  @override
  userImage(File _image) {
    setState(() {
      this.images[pageIndex] = _image;
    });
  }
}

class DrawingPoints {
  Paint paint;
  Offset points;
  double size;

  DrawingPoints({this.points, this.paint, this.size});
  String toString() => paint.color.toString() + "*" + paint.strokeWidth.toString() + "*" + points.toString() + "*" +size.toString();
}

class DrawingPainter extends CustomPainter {
  DrawingPainter({this.pointsList});
  List<DrawingPoints> pointsList;

  @override
  void paint(Canvas canvas, Size size) {
    for (int i = 0; i < pointsList.length-1 ;i++) {
      if(pointsList[i] == null) continue;  // 없는 점이면 그냥 통과
      // i번쨰는 null 이 아님.
      if(pointsList[i+1] == null) {     // 불연속 점인 경우 ( i 는 null 이 아니고 i+1 은 null)
        int j = i + 1;
        while (pointsList[j] == null &&  j != pointsList.length-1) { // 그 다음 NULL 이 아닌 점을 찾아 봄
          j++;
        }
        i = j - 1;
        continue;
      }
      // i 와 i+1 이 둘다 null 이 아닌 경우
      canvas.drawLine(pointsList[i].points, pointsList[i + 1].points, pointsList[i].paint);
    }
  }
  @override
  bool shouldRepaint(DrawingPainter oldDelegate) => true;
}

class RectanglePainter extends CustomPainter{
  RectanglePainter({this.rectanglePoints,this.movePoints});
  List<Offset> rectanglePoints;
  List<Offset> movePoints;
  @override
  void paint(Canvas canvas, Size size) {
    if(rectanglePoints.length == 0){
      return;
    }
    int index = rectanglePoints.length -1 ;
    double x1 = rectanglePoints[0].dx;
    double x2 = rectanglePoints[index].dx;
    double y1 = rectanglePoints[0].dy;
    double y2 = rectanglePoints[index].dy;
    double dx = 0;
    double dy = 0;
    Paint defaultPaint =Paint()
      ..strokeCap =StrokeCap.round
      ..isAntiAlias = true
      ..color = Colors.black
      ..strokeWidth = 1.0
      ..style = PaintingStyle.stroke;


    Path path =  new Path();
    if(movePoints !=null && movePoints.length != 0 ) {
      dx = movePoints[movePoints.length-1].dx;
      dy = movePoints[movePoints.length-1].dy;
    }

    path.moveTo(x1+dx,y1+dy);
    path.lineTo(x2+dx,y1+dy);
    path.lineTo(x2+dx,y2+dy);
    path.lineTo(x1+dx,y2+dy);
    path.lineTo(x1+dx,y1+dy);
    canvas.drawPath(dashPath(path, dashArray: CircularIntervalList<double>(<double>[10, 10]),),defaultPaint);


  }

  @override
  bool shouldRepaint(RectanglePainter oldDelegate) => true;
}




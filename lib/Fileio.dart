import 'package:path_provider/path_provider.dart';
import 'package:flutter/material.dart';
import 'dart:io';


class Fileio{
  //File io
  final String name;

  Fileio({Key key, @required this.name});

//  void setname (String val) {
//    name= val;
//  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/$name.txt');
  }

  Future<File> writeContents(String task) async {
    final file = await _localFile;
    // Write the file.
    debugPrint('write path: $file');
    return file.writeAsString('$task');
  }

  Future<String> readContents() async {
    try {
      final file = await _localFile;
      print(file);
      // Read the file.
      String contents = await file.readAsString();
      return contents;
    } catch (e) {
      // If encountering an error, return 0.
      return null;
    }
  }

  Future<File> addTodoFuture(String task, List<String> _todoItems) { //새거 입력할 때 마다 호출
    String text='';

//    setState(() {
      _todoItems.add(task);
//    });
    for (var i = 0; i < _todoItems.length; i++) {
      var o = _todoItems[i];
      text+='\\'+o;
      debugPrint('for loop: $text');
    }
    debugPrint('add: $text'+task);
    // Write the variable as a string to the file.
    text = text.replaceAll(' ', '_');
    return writeContents(text); //text파일에 들어가는 형태: "1\2\3\...\N"(=task)
  }

  Future<File> deleteTodoFuture(String task, List<String> _todoItems) {
    String text='';
//    setState(() {
      _todoItems.remove(task);
      debugPrint('after remove: $_todoItems');

//    });
    for (var i = 0; i < _todoItems.length; ++i) {
      var o = _todoItems[i];
      if (o != task) {
        text+='\\'+o;
      }
      debugPrint('after delete: $text');
    }
    // Write the variable as a string to the file.
    text = text.replaceAll(' ', '_');
    return writeContents(text); //text파일에 들어가는 형태: "1\2\3\...\N"(=task)
  }

  Future<File> editTodoFuture(String task, int index, List<String> _todoItems) {
    String text='';
    String tmp = '';
//    setState(() {
      tmp=_todoItems[index];
      _todoItems.remove(tmp);
      _todoItems.insert(index, task);
//    });
    for (var i = 0; i < _todoItems.length; i++) {
      var o = _todoItems[i];
      text+='\\'+o;
      debugPrint('for loop: $text');
    }
    debugPrint('add: $text'+task);
    // Write the variable as a string to the file.
    text = text.replaceAll(' ', '_');
    return writeContents(text); //text파일에 들어가는 형태: "1\2\3\...\N"(=task)
  }
}

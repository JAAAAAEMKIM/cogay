import 'package:flutter/material.dart';
import 'dart:io';
import 'paint.dart';
import 'Fileio.dart';
import 'package:dio/dio.dart';
import 'swipe_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:drawing_space/http.dart';


class InputForm extends StatefulWidget {
  final String userid;
  final List<String> items;
  final String channel;
  final int index;
  final Function(String, int) notifyParent;
  List<String> list=null;

  void Function(String s) onSubmit = InputFormState()._onSubmit;

  InputForm({Key key, this.userid, @required this.items, @required this.index, @required this.notifyParent, @required this.channel}) : super(key: key);

  @override
  createState() => new InputFormState();
}

class InputFormState extends State<InputForm> {
  TextEditingController _text = TextEditingController();
  bool _validate = false;
  String submitStr = "";
  List<String> _todoItems;
  int index;
//  String CH_NAME;
  String USERID;
  MyHttpRequests http = MyHttpRequests();

  @override
  void initState() {
    super.initState();
//    CH_NAME = widget.channel.split('|')[1];
//    debugPrint('inputform chname: $CH_NAME');
    _todoItems = widget.items;
    index = widget.index;
    USERID = widget.userid;

    debugPrint('userid: $USERID , $widget.userid');
    debugPrint('$index');
  }
  void _changeText(String val) {
    setState(() {
      submitStr = val;
    });

    print("On RaisedButton : The text is $submitStr");
  }

  void _onSubmit(String val) {
    print("OnSubmit : The text is $val");
    setState(() {
      submitStr = val;
    });
  }

  void _onChanged(String value) {
    print('"OnChange : " $value');
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
          child:AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(24.0))),
            content: new Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
              Text('Input New Name'),
              TextField(
                controller: index>=1000007? _text= new TextEditingController():_text = new TextEditingController(text: _todoItems[index]),

                autofocus: true,
                decoration: new InputDecoration(
                  hintText: 'Name',
                  contentPadding: const EdgeInsets.all(16.0),
                  errorText: _validate ? 'Name already exists.' : null,
                ),
                onChanged: (String value) {
                  _onChanged(value);
                },
                onSubmitted: (String submittedStr) {
                  _onSubmit(submittedStr);
                  _text.text = "";
                },

              ),
              new Text('$submitStr'),
              new RaisedButton(
                shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(15.0)),
                child: new Text("Submit"),
                onPressed: () {
                _changeText(_text.text);
                String val = _text.text;

                if(checkval(val, _todoItems)){
                  if(index == 1000007){
//                    widget.storage.addTodoFuture(val, _todoItems);
                    setState(() async {
//                      _todoItems.add(val);
                      final prefs = await SharedPreferences.getInstance();
                      String username = prefs.getString('fname');
                      debugPrint('newchannel/$USERID/$username/$val');
                      Response tmp = await http.postHttpRequest('newchannel/$USERID/$username/$val', '').then((tmp)async{
                        setState(() {
//                          _todoItems.add(val);
                          widget.notifyParent(val, 1000007);
                          Navigator.pop(context);
                        });

                        return null;
                      });
                    });
                  }
                  else if(index == 1000008){
                    setState(() {
                      http.postHttpRequest('newroom/${widget.channel}/$val', '');
                      debugPrint('newroom/${widget.channel}/$val');
                      widget.notifyParent(val, 1000008);
                      Navigator.pop(context);
                    });
                  }
                  else{ //edit
                    http.putHttpRequest('editchannel/${widget.channel}/$val');

                    setState(() {
                      widget.notifyParent(val, index);
                    });
                    Navigator.pop(context);
                  }

                }// Close the add todo screen
                else{
                _validate=true;
                }

                _text.text="";
                },
                textColor: Colors.white,
                color: Colors.orangeAccent,
//                hoverColor: Colors.black,
              )
            ]
        )
      )
    );
  }
  bool checkval(String val, List<String> list){
    bool answer = true;
    if(val.length==0){
      answer = false;
    }
    else if(ValisinList(val, list)){
      answer = false;
    }
    debugPrint('answer:$answer');
    return answer;
  }

  bool ValisinList(String val, List<String> list){
    bool answer = false;
    for (var i = 0; i < list.length; ++i) {
      var o = list[i];

      if(val == o){
        answer = !answer;
      }
    }
    return answer;
  }
}
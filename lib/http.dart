import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class MyHttpRequests{
  Future<Response> RESTAPI(int index, String req, String _data) async {
    Response response;
    Dio dio = new Dio();
    await DotEnv().load('.env');
    dio.options.baseUrl = DotEnv().env['CURRENT_IP'];
    dio.options.connectTimeout = 5000; //5s
    dio.options.receiveTimeout = 3000;

    switch(index){
      case(0): //GET
        try {
          response = await dio.get('/$req');
          debugPrint('res: $response');
          return response;
        } catch (e) {
          debugPrint('res: FAILED to get');
          print(e);
          return null;
        }
        break;
      case(1): //POST
        try {
          response = await dio.post('/$req', data: _data);
          debugPrint('res: $response');
          return response;
        } catch (e) {
          debugPrint('res: FAILED to post');
          print(e);
          return null;
        }
        break;

      case(2): //DELETE
        try {
          response = await dio.delete('/$req');
          debugPrint('res: $response');
          return response;
        } catch (e) {
          debugPrint('res: FAILED to delete');
          print(e);
          return null;
        }
        break;
      case(3): //PUT
        try {
          response = await dio.put('/$req');
          debugPrint('res: $response');
          return response;
        } catch (e) {
          debugPrint('res: FAILED to put');
          print(e);
          return null;
        }
        break;
    }

  }

  Future<Response> makeHttpRequest(String req) async { //GET
    return RESTAPI(0, req, null);
  }
  Future<Response> postHttpRequest(String req, String _data) async {
    return RESTAPI(1, req, _data);
  }
  Future<Response> deleteHttpRequest(String req) async {
    return RESTAPI(2, req, null);
  }
  Future<Response> putHttpRequest(String req) async {
    return RESTAPI(3, req, null);
  }
//    debugPrint('try');
//    Response response;
//    Dio dio = new Dio();
//    await DotEnv().load('.env');
//    dio.options.baseUrl = DotEnv().env['CURRENT_IP'];
//    dio.options.connectTimeout = 5000; //5s
//    dio.options.receiveTimeout = 3000;
//    try {
//      response = await dio.get('/$req');
//      debugPrint('res: $response');
//      return response;
//    } catch (e) {
//      debugPrint('res: FAILED');
//      print(e);
//      return null;
//    }


//    debugPrint('try');
//    Response response;
//    Dio dio = new Dio();
//    await DotEnv().load('.env');
//    dio.options.baseUrl = DotEnv().env['CURRENT_IP'];
//    dio.options.connectTimeout = 5000; //5s
//    dio.options.receiveTimeout = 3000;
//    try {
//      response = await dio.post('/$req', data: _data);
//      debugPrint('res: $response');
//      return response;
//    } catch (e) {
//      debugPrint('res: FAILED');
//      print(e);
//      return null;
//    }
//  }

}

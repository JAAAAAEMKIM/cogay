import 'dart:async';
import 'dart:io';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:drawing_space/paint.dart';
import 'package:drawing_space/image_picker_dialog.dart';
import 'package:image_picker/image_picker.dart';

class ImagePickerHandler {
  ImagePickerDialog imagePicker;
  AnimationController _controller;
  ImagePickerListener _listener;


  ImagePickerHandler(this._listener, this._controller);

  openCamera() async {            // async로 선언한 함수들은 비동기 함수 . > 새로운 thread를 만들어서 실행.
    imagePicker.dismissDialog();
    var image = await ImagePicker.pickImage(source: ImageSource.camera);    // image를 카메라에서 가져오고
    if(image != null) cropImage(image);             // cropping 함.
  }

  openGallery() async {
    imagePicker.dismissDialog();
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    if(image != null) cropImage(image);
  }

  void init() {
    imagePicker = new ImagePickerDialog(this, _controller);
    imagePicker.initState();
  }

  Future cropImage(File image) async {
    _listener.userImage(image);
  }

  showDialog(BuildContext context) {
    imagePicker.getImage(context);
  }
}

abstract class ImagePickerListener {
  userImage(File _image);
}

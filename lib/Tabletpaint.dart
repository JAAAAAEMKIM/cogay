import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'dart:ui';
import 'dart:convert';
import 'dart:collection';
import 'package:dio/dio.dart';
import 'package:drawing_space/GridView.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:photo_view/photo_view.dart';
import 'package:drawing_space/image_picker_handler.dart';
import 'package:drawing_space/image_picker_dialog.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:web_socket_channel/status.dart' as status;
import 'package:path_drawing/path_drawing.dart';
import 'package:photo_view/photo_view.dart';
import 'package:drawing_space/PageBody.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'Fileio.dart';
import 'http.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';


class Draw extends StatefulWidget {
  WebSocketChannel wsgrid;
  final WebSocketChannel wschannel = IOWebSocketChannel.connect(DotEnv().env['WS_IP_C']);
  final WebSocketChannel wschannel2 = IOWebSocketChannel.connect(DotEnv().env['WS_IP_D']);
  final String channel_;
  final String room;
  Fileio storage;
  String userid;
  String channelname;
  String username;

  Draw({Key key, this.wsgrid, this.username, this.channelname, @required this.channel_, this.userid, @required this.room}): super(key: key);

  @override
  _DrawState createState() => _DrawState();
}




class _DrawState extends State<Draw> with TickerProviderStateMixin {

  Color selectedColor = Colors.black;
  Color pickerColor = Colors.black;

  bool showBottomList = false;
  bool showBottomNavigationBar = false;
  bool colorChoose = false;
  bool paperChoose = false;
  bool pageChoose = false;


  double scrollableButtonHeight =60;
  double strokeWidth = 3.0;
  double opacity = 1.0;
  StrokeCap strokeCap = StrokeCap.round;
  SelectedMode selectedMode = SelectedMode.StrokeWidth;
  String user_id;
  String channelname;
  String username;
  IOWebSocketChannel wschannel;




  List<Color> colors = [Colors.red, Colors.green, Colors.blue, Colors.amber, Colors.black];
  Color buttonPressedColor = Colors.deepOrange;

  Color penButtonColor = Colors.white;
  Color sizeButtonColor = Colors.white;
  Color eraseButtonColor = Colors.white;
  Color cutButtonColor = Colors.white;
  Color colorButtonColor = Colors.white;
  Color fileButtonColor = Colors.white;
  Color refreshButtonColor = Colors.white;
  Color backGroundButtonColor = Colors.white;
  Color undoButtonColor = Colors.white;
  Color redoButtonColor = Colors.white;



  List<PageBody> pageBodyList;

  int currentIndex = 0;
  int newIndex;
  AnimationController _controller;
  int pageIndex = 0;
  List<PageBody> pages = List();
  final controller = PageController(initialPage: 1);
  String buffer;


  void _sendDataGrid(String event) {
    widget.wsgrid.sink.add(
        jsonEncode({
          'event': event,
          'channel': widget.channel_,
          'room': widget.room,
          'data': '',
          'nickname':widget.username+'|'+widget.userid,
          'page':0,
        })
    );
  }

  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    BackButtonInterceptor.add(myInterceptor2);
    _sendData("connect room paint");
    user_id = widget.userid;
    channelname = widget.channelname;
    pageBodyList = [ PageBody(channel_: widget.channel_, username: widget.username,room: widget.room, wschannel: widget.wschannel, wschannel2: widget.wschannel2, userid: widget.userid, channelname: widget.channelname)];


//    try {
//      widget.wschannel.stream.listen((data) {
//        print("channel 1 listen start");
//        var a = jsonDecode(data);
//        print(a["event"]);
//        switch (a["event"]) {
//          case "draw end draw":
//            buffer = '${a["data"]}';
//            pageBodyList[0].getBuffer(buffer);
//            decode(a["data"],temp);
//            pageBodyList[0].setOtherPage(temp,a["page"]);
//            break;
//          case "draw start":
//            break;
//          case "connect":
//            break;
//          case "data":
//            decodeAll(a["data"],listenPagePointList);
//            pageBodyList[0].setAllOtherPage(listenPagePointList);
//            break;
//          case "leave":
//            break;
//        }
//      });
//
//      print('wschannel is ${widget.wschannel}');
//    }catch(e){
//
//      print("Already listening");
//    }





    try {
      widget.wschannel2.stream.listen((data) {
        print("channel 2 listen start");
        var a = jsonDecode(data);
        print(a["event"]);
        switch (a["event"]) {
          case "draw end paint":
            break;
          case "draw start paint":
            break;
          case "connect room paint":
            break;
          case "leave room paint":
            break;
          case "data":
            decodeAll(a["data"],listenPagePointList);
            pageBodyList[0].setAllOtherPage(listenPagePointList);
            break;
        }
      });

      print('wschannel is ${widget.wschannel}');
    }catch(e){

      print("Already listening");
    }

  }

  @override
  void dispose() {
    _controller.dispose();
//    WidgetsBinding.instance.removeObserver(this);
//    widget.wschannel.sink.close();

//backbutton
    BackButtonInterceptor.remove(myInterceptor2);
    super.dispose();
  }

//  @override
//  void didChangeAppLifecycleState(AppLifecycleState state) {
//    print("------------------------------------------");
//    print(state);
//
//    if(state == AppLifecycleState.resumed){
//      // user returned to our app
//    }else if(state == AppLifecycleState.inactive){
//      // app is inactive
//    }else if(state == AppLifecycleState.paused){
//      // user quit our app temporally
//    }else if(state == AppLifecycleState.suspending){
//      _sendData("test");
//    }
//  }

  void _sendData(String event) {
    widget.wschannel.sink.add(
        jsonEncode({
          'event': event,
          'channel': widget.channel_,
          'room': widget.room,
          'data': '',
          'nickname':widget.username+'|'+widget.userid,
          'page':0,
        })
    );
  }
  void setColorsWhite(){
    penButtonColor = Colors.white;
    sizeButtonColor = Colors.white;
    eraseButtonColor = Colors.white;
    cutButtonColor = Colors.white;
    colorButtonColor = Colors.white;
    fileButtonColor = Colors.white;
    refreshButtonColor = Colors.white;
    backGroundButtonColor = Colors.white;
    undoButtonColor = Colors.white;
    redoButtonColor = Colors.white;
  }

  void navigateBack() async{
    BackButtonInterceptor.remove(myInterceptor2);
//    MyHttpRequests _http = MyHttpRequests();
//    Response res = await _http.makeHttpRequest('showroom/${widget.channel_}');
    _sendData('leave room paint');
    _sendDataGrid('leave room grid');
    widget.wschannel.sink.close();
    widget.wschannel2.sink.close();
    Navigator.pop(context, false);
    BackButtonInterceptor.remove(myInterceptor2);

    BackButtonInterceptor.removeAll();
  }

  bool myInterceptor2 (bool stopDefaultButtonEvent){
//    BackButtonInterceptor.remove(myInterceptor);
    BackButtonInterceptor.removeAll();
    print("BACK BUTTON!"); // Do some stuff.
    debugPrint("backbutton pressed");

//    GetResponse();
    navigateBack();
    return true;
  }

  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: appBar(),
//      persistentFooterButtons: scrollableButtons(),
      bottomNavigationBar: bottomNavigationBar(),      // color를 선택하지 않을 떄
      body: pageBody(),
      floatingActionButton: StackExample(wschannel: widget.wschannel, notify: refresh),//peopleIcons(),
      extendBody: true,
    );
  }

  void refresh(){
    setState((){

    });
  }

  Widget pageBody(){
    return pageBodyList[0];
//    return pageBodyList[pageIndex];
  }

  Widget bottomNavigationBar(){
    if(colorChoose) return colorChooseBar();
    else if(paperChoose) return paperChooseBar();
    else if(pageChoose) return pageChooseBar();
    return undoEraseRedoBar();

  }
  Widget paperChooseBar(){
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child:
      Container(
          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50.0),
              color: Color.fromRGBO(252, 204, 121, 1)),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(child: Flexible(
                          child: FlatButton(
                              onPressed: () {
                                setState(() {
                                  pageBodyList[0].changePaperNumber(0);
                                });
                              },
                              padding: EdgeInsets.all(0.0),
                              child: Image.asset(
                                'assets/whitepapericon.png',
                                height: MediaQuery.of(context).size.width/6,
                                width:  MediaQuery.of(context).size.width/6,

                              )
                          ))),
                      Container(child: Flexible(
                          child: FlatButton(
                              onPressed:() {
                                setState(() {
                                  pageBodyList[0].changePaperNumber(1);
                                });
                              },
                              padding: EdgeInsets.all(0.0),
                              child: Image.asset(
                                'assets/legalpadicon.png',
                                height: MediaQuery.of(context).size.width/6,
                                width:  MediaQuery.of(context).size.width/6,

                              )
                          ))),
                      Container(child: Flexible(
                          child: FlatButton(
                              onPressed: () {
                                setState(() {
                                  pageBodyList[0].changePaperNumber(2);
                                });
                              },
                              padding: EdgeInsets.all(0.0),
                              child: Image.asset(
                                'assets/graphpaper.png',
                                height: MediaQuery.of(context).size.width/6,
                                width:  MediaQuery.of(context).size.width/6,

                              )
                          ))),

                      Container(child: Flexible(
                          child: FlatButton(
                              onPressed: () {
                                setState(() {
                                  pageBodyList[0].changePaperNumber(3);
                                });
                              },
                              padding: EdgeInsets.all(0.0),
                              child: Image.asset(
                                'assets/gridpapericon.png',
                                height: MediaQuery.of(context).size.width/6,
                                width:  MediaQuery.of(context).size.width/6,

                              )
                          ))),
                    ]
                ),
              ],
            ),
          )
      ),
    );
  }
  Widget horizontalBlock(){
    return Container(
      width: 10,
    );
  }
  Widget pageChooseBar(){
    Color buttonColor = Colors.white;
    Color barColor = Color.fromRGBO(252, 204, 121, 1);
    Color textColor = Colors.black;
    double minWidth = 50;
    double fontSize = 15;

    return new Padding(
      padding: const EdgeInsets.all(10.0),
      child:
      Container(
          height: 60,
          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50.0),
              color: barColor),
          child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: ListView(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  Container(child:
                  ButtonTheme(
                    minWidth: minWidth,
                    buttonColor: buttonColor,
                    child: RaisedButton(
                      child: Text(
                        '1',
                        textScaleFactor: 1,
                        style: TextStyle(fontWeight: FontWeight.bold ,color:textColor,fontSize: fontSize),
                      ),
                      onPressed: () {
                        setState(() {
                          pageBodyList[0].changePage(0);
                          pageChoose =false;
                        });
                      },
                    ),)

                  ),  //1
                  horizontalBlock(),
                  Container(child:
                  ButtonTheme(
                    buttonColor: buttonColor,
                    minWidth: minWidth,
                    child: RaisedButton(
                      child: Text(
                        '2',
                        textScaleFactor: 1,
                        style: TextStyle(fontWeight: FontWeight.bold ,color: textColor,fontSize: fontSize),
                      ),
                      onPressed: () {
                        setState(() {
                          pageBodyList[0].changePage(1);
                          pageChoose =false;
                        });
                      },
                    ),)

                  ),
                  horizontalBlock(),//2
                  Container(child:
                  ButtonTheme(
                    buttonColor: buttonColor,
                    minWidth: minWidth,
                    child: RaisedButton(
                      child: Text(
                        '3',
                        textScaleFactor: 1,
                        style: TextStyle(fontWeight: FontWeight.bold ,color: textColor,fontSize: fontSize),
                      ),
                      onPressed: () {
                        setState(() {
                          pageBodyList[0].changePage(2);
                          pageChoose =false;
                        });
                      },
                    ),)

                  ),
                  horizontalBlock(),
                  Container(child:
                  ButtonTheme(
                    buttonColor: buttonColor,
                    minWidth: minWidth,
                    child: RaisedButton(
                      child: Text(
                        '4',
                        textScaleFactor: 1,
                        style: TextStyle(fontWeight: FontWeight.bold ,color: textColor,fontSize: fontSize),
                      ),
                      onPressed: () {
                        setState(() {
                          pageBodyList[0].changePage(3);
                          pageChoose =false;
                        });
                      },
                    ),)

                  ),
                  horizontalBlock(),
                  Container(child:
                  ButtonTheme(
                    buttonColor: buttonColor,
                    minWidth: minWidth,
                    child: RaisedButton(
                      child: Text(
                        '5',
                        textScaleFactor: 1,
                        style: TextStyle(fontWeight: FontWeight.bold ,color: textColor,fontSize: fontSize),
                      ),
                      onPressed: () {
                        setState(() {
                          pageBodyList[0].changePage(4);
                          pageChoose =false;
                        });
                      },
                    ),)

                  ),
                  horizontalBlock(),
                  Container(child:
                  ButtonTheme(
                    buttonColor: buttonColor,
                    minWidth: minWidth,
                    child: RaisedButton(
                      child: Text(
                        '6',
                        textScaleFactor: 1,
                        style: TextStyle(fontWeight: FontWeight.bold ,color: textColor,fontSize: fontSize),
                      ),
                      onPressed: () {
                        setState(() {
                          pageBodyList[0].changePage(5);
                          pageChoose =false;
                        });
                      },
                    ),)

                  ),
                  horizontalBlock(),
                  Container(child:
                  ButtonTheme(
                    buttonColor: buttonColor,
                    minWidth: minWidth,
                    child: RaisedButton(
                      child: Text(
                        '7',
                        textScaleFactor: 1,
                        style: TextStyle(fontWeight: FontWeight.bold ,color: textColor,fontSize: fontSize),
                      ),
                      onPressed: () {
                        setState(() {
                          pageBodyList[0].changePage(6);
                          pageChoose =false;
                        });
                      },
                    ),)

                  ),
                  horizontalBlock(),
                  Container(child:
                  ButtonTheme(
                    buttonColor: buttonColor,
                    minWidth: minWidth,
                    child: RaisedButton(
                      child: Text(
                        '8',
                        textScaleFactor: 1,
                        style: TextStyle(fontWeight: FontWeight.bold ,color: textColor,fontSize: fontSize),
                      ),
                      onPressed: () {
                        setState(() {
                          pageBodyList[0].changePage(7);
                          pageChoose =false;
                        });
                      },
                    ),)

                  ),
                  horizontalBlock(),
                  Container(child:
                  ButtonTheme(
                    buttonColor: buttonColor,
                    minWidth: minWidth,
                    child: RaisedButton(
                      child: Text(
                        '9',
                        textScaleFactor: 1,
                        style: TextStyle(fontWeight: FontWeight.bold ,color: textColor,fontSize: fontSize),
                      ),
                      onPressed: () {
                        setState(() {
                          pageBodyList[0].changePage(8);
                          pageChoose =false;
                        });
                      },
                    ),)

                  ),
                  horizontalBlock(),
                  Container(child:
                  ButtonTheme(
                    buttonColor: buttonColor,
                    minWidth: minWidth,
                    child: RaisedButton(
                      child: Text(
                        '10',
                        textScaleFactor: 1,
                        style: TextStyle(fontWeight: FontWeight.bold ,color: textColor,fontSize: fontSize),
                      ),
                      onPressed: () {
                        setState(() {
                          pageBodyList[0].changePage(9);
                          pageChoose =false;
                        });
                      },
                    ),)

                  ),
                  horizontalBlock(),
                  Container(child:
                  ButtonTheme(
                    buttonColor: buttonColor,
                    minWidth: minWidth,
                    child: RaisedButton(
                      child: Text(
                        '11',
                        textScaleFactor: 1,
                        style: TextStyle(fontWeight: FontWeight.bold ,color:textColor,fontSize: fontSize),
                      ),
                      onPressed: () {
                        setState(() {
                          pageBodyList[0].changePage(10);
                          pageChoose =false;
                        });
                      },
                    ),)

                  ),
                  horizontalBlock(),
                  Container(child:
                  ButtonTheme(
                    buttonColor: buttonColor,
                    minWidth: minWidth,
                    child: RaisedButton(
                      child: Text(
                        '12',
                        textScaleFactor: 1,
                        style: TextStyle(fontWeight: FontWeight.bold ,color: textColor,fontSize: fontSize),
                      ),
                      onPressed: () {
                        setState(() {
                          pageBodyList[0].changePage(11);
                          pageChoose =false;
                        });
                      },
                    ),)

                  ),
                  horizontalBlock(),
                  Container(child:
                  ButtonTheme(
                    buttonColor: buttonColor,
                    minWidth: minWidth,
                    child: RaisedButton(
                      child: Text(
                        '13',
                        textScaleFactor: 1,
                        style: TextStyle(fontWeight: FontWeight.bold ,color: textColor,fontSize: fontSize),
                      ),
                      onPressed: () {
                        setState(() {
                          pageBodyList[0].changePage(12);
                          pageChoose =false;
                        });
                      },
                    ),)

                  ),
                  horizontalBlock(),
                  Container(child:
                  ButtonTheme(
                    buttonColor: buttonColor,
                    minWidth: minWidth,
                    child: RaisedButton(
                      child: Text(
                        '14',
                        textScaleFactor: 1,
                        style: TextStyle(fontWeight: FontWeight.bold ,color: textColor,fontSize: fontSize),
                      ),
                      onPressed: () {
                        setState(() {
                          pageBodyList[0].changePage(13);
                          pageChoose =false;
                        });
                      },
                    ),)

                  ),
                  horizontalBlock(),
                  Container(child:
                  ButtonTheme(
                    buttonColor: buttonColor,
                    minWidth: minWidth,
                    child: RaisedButton(
                      child: Text(
                        '15',
                        textScaleFactor: 1,
                        style: TextStyle(fontWeight: FontWeight.bold ,color: textColor,fontSize: fontSize),
                      ),
                      onPressed: () {
                        setState(() {
                          pageBodyList[0].changePage(14);
                          pageChoose =false;
                        });
                      },
                    ),)

                  ),

                ],
              )
          )
      ),
    );
  }


  List<String> peopleOnSamePage = ["kim","jun","bum","wsw"];
  Widget peopleIcons(){
    // peopleOnSamePage를 서버에서 받아온 걸로 replace만 하면 됨

    int peopleNumber = peopleOnSamePage.length;
    if(peopleNumber == 0){
      return horizontalBlock();
    }
    if( peopleNumber == 1){
      return new Container(
        padding: const EdgeInsets.all(8.0),
        // alignment: FractionalOffset.center,
        child: new Stack(
          alignment:new Alignment(0, 0),
          children: <Widget>[

            Positioned(
                left: 10,//
                top: 170,
                child: new CircleAvatar(backgroundColor: Colors.orangeAccent,radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 17,
                    child: new Text(peopleOnSamePage[0])))

            ),

          ],
        ),
      );
    }
    else if(peopleNumber == 2){
      return new Container(
        padding: const EdgeInsets.all(8.0),
        // alignment: FractionalOffset.center,
        child: new Stack(
          alignment:new Alignment(0, 0),
          children: <Widget>[

            Positioned(
                left: 10,//
                top: 170,
                child: new CircleAvatar(backgroundColor: Colors.orangeAccent,radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 17,
                    child: new Text(peopleOnSamePage[0])))

            ),


            Positioned(
                left: 35,
                top: 170,
                child: new CircleAvatar(backgroundColor: Colors.orange,radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 17,
                    child: new Text(peopleOnSamePage[1])))
            ),
          ],
        ),
      );
    }
    else if(peopleNumber >= 3){
      return new Container(
        padding: const EdgeInsets.all(8.0),
        // alignment: FractionalOffset.center,
        child: new Stack(
          alignment:new Alignment(0, 0),
          children: <Widget>[

            Positioned(
                left: 10,//
                top: 170,
                child: new CircleAvatar(backgroundColor: Colors.orangeAccent,radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 17,
                    child: new Text(peopleOnSamePage[0])))

            ),


            Positioned(
                left: 35,
                top: 170,
                child: new CircleAvatar(backgroundColor: Colors.orange,radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 17,
                    child: new Text(peopleOnSamePage[1])))
            ),

            Positioned(
                left: 60,
                top: 170,
                child: new CircleAvatar(backgroundColor: Colors.deepOrangeAccent,radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 17,
                    child: new Text(peopleOnSamePage[2])))

            ),
          ],
        ),
      );
    }




  }


  List<File> allPageImages = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,];
  File pageImage;






  // scaffold 관련 Widget
  Widget appBar(){
    return new AppBar(       // AppBar 부분 단순히 Text만 가짐
      backgroundColor:  Color.fromRGBO(252, 204, 121, 1),
      automaticallyImplyLeading: false,
      leading: IconButton(icon:Icon(Icons.arrow_back, color: Colors.white,),
        onPressed:() {Navigator.pop(context, false);
        _sendData('leave room paint');
        _sendDataGrid('leave room grid');
        widget.wschannel.sink.close();},
        color: Colors.black,
      ),
      title: Container(
        child:Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(children: <Widget>[
              sizeButton(),
              paperButton(),
              fileButton(),]),
            Row(children: <Widget>[
              penButton(),
              colorButton(),
              eraserButton(),
              cutButton(),]),

            refreshButton(),
            Row(children: <Widget>[
              undoButton(),
              redoButton(),]),
          ],),
      ),
//      flexibleSpace:
    );
  }


  Widget colorChooseBar(){
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child:
      Container(
          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50.0),
              color: Colors.greenAccent),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,

              children: <Widget>[
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.album),
                          iconSize: 30,
                          onPressed: () {
                            setState(() {
                              if (selectedMode == SelectedMode.StrokeWidth)
                                showBottomList = !showBottomList;
                              selectedMode = SelectedMode.StrokeWidth;
                            });
                          }),
                      IconButton(
                          icon: Icon(Icons.opacity),
                          iconSize: 30,
                          onPressed: () {
                            setState(() {
                              if (selectedMode == SelectedMode.Opacity)
                                showBottomList = !showBottomList;
                              selectedMode = SelectedMode.Opacity;
                            });
                          }),
                      IconButton(
                          icon: Icon(Icons.color_lens),
                          iconSize: 30,
                          onPressed: () {
                            setState(() {
                              if (selectedMode == SelectedMode.Color)
                                showBottomList = !showBottomList;
                              selectedMode = SelectedMode.Color;
                            });
                          }),
                      IconButton(
                          icon: Icon(Icons.clear),
                          iconSize: 30,
                          onPressed: () {
                            setState(() {
                              showBottomList = false;
                              toggleColor();
                            });
                          }),
                    ]
                ),
                Visibility(
                  child: (selectedMode == SelectedMode.Color)   //색 선택인 경우 getColorList()를 불러옴
                      ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: getColorList(),
                  )
                      : Slider(       // 색 선택이 아닌 경우 Slider를 불러옴 >
                      value: (selectedMode == SelectedMode.StrokeWidth)
                          ? strokeWidth
                          : opacity,
                      max: (selectedMode == SelectedMode.StrokeWidth)
                          ? 50.0
                          : 1.0,
                      min: 0.0,
                      onChanged: (val) {
                        setState(() {
                          if (selectedMode == SelectedMode.StrokeWidth) {
                            pageBodyList[0].setStrokeWidth(val);
                            strokeWidth = val;
                          }
                          else {
                            pageBodyList[0].setOpacity(val);
                            opacity = val;
                          }
                        });
                      }),
                  visible: showBottomList,
                ),
              ],
            ),
          )
      ),
    );
  }
  Widget undoEraseRedoBar(){

    return new ButtonBar(
      alignment:
      MainAxisAlignment.center,
      children: <Widget>[
        beforePageButton(),
        pageText(),
        nextPageButton(),
      ],
    );
  }
  //color Choose Bar 버튼
  Widget beforePageButton(){
    return new ButtonTheme(
        child: new IconButton(
            icon:Icon(Icons.chevron_left,size: 30.0),
            onPressed: () {
              setState(() {
                pageBodyList[pageIndex].formerPage();
              });
            }
        )
    );
  }
  Widget nextPageButton(){
    return new ButtonTheme(
        child: new IconButton(
            icon:Icon(Icons.chevron_right,size: 30.0,),
            onPressed: () {
              setState(() {
                pageBodyList[pageIndex].nextPage();
              });
            }
        )
    );


  }
  Widget eraserButton(){
    return new ButtonTheme(
        child: new IconButton(
            icon:Icon(Icons.crop_square,size: 30.0,color:  eraseButtonColor,),
            onPressed: () {
              setState(() {
                pageBodyList[pageIndex].setEraser();
                setColorsWhite();
                eraseButtonColor = buttonPressedColor;
              });
            }
        )
    );
  }
  Widget pageText(){
    int pageNumber = pageBodyList[0].getPageNumber();

    return RaisedButton(
      color: Colors.white,
      child:
      Text(
        ' $pageNumber/15 ',
        textScaleFactor: 1,
        style: TextStyle(fontWeight: FontWeight.w400 ,color: Colors.black,fontSize: 20),
      ),
      onPressed: () {
        setState(() {
          togglePageChoose();
        });
      },
    );
  }
  Widget redoButton(){
    return new ButtonTheme(
        child: new IconButton(
            icon:Icon(Icons.redo,size: 30.0,color: redoButtonColor),
            onPressed: () {
              setState(() {
                pageBodyList[pageIndex].redo();
                setColorsWhite();
                redoButtonColor = buttonPressedColor;
              });
            }
        )
    );
  }
  Widget undoButton(){
    return new ButtonTheme(
        child: new IconButton(
            icon:Icon(Icons.undo,size: 30.0,color: undoButtonColor),
            onPressed: () {
              setState(() {
                pageBodyList[pageIndex].undo();
                setColorsWhite();
                undoButtonColor = buttonPressedColor;
              });
            }
        )
    );
  }




  //AppBar Button
  Widget sizeButton(){
    return new ButtonTheme(
        child: new IconButton(
            icon:Icon(Icons.filter_center_focus,size: 30.0,color: sizeButtonColor,),
            onPressed: () {
              setState(() {
                pageBodyList[pageIndex].setScaleMode();
                setColorsWhite();
                sizeButtonColor = buttonPressedColor;
              });
            }
        )
    );
  }
  Widget cutButton(){
    return new ButtonTheme(
        child: new IconButton(
            icon:Icon(Icons.content_cut,size: 30.0,color: cutButtonColor,),
            onPressed: () {
              setState(() {
                pageBodyList[pageIndex].setCutMode();
                setColorsWhite();
                cutButtonColor = buttonPressedColor;
              });
            }
        )
    );
  }
  Widget penButton(){
    return new ButtonTheme(
      child: new IconButton(
          icon:Icon(Icons.mode_edit,size: 30.0,color: penButtonColor,),
          onPressed: () {
            setState(() {
              pageBodyList[pageIndex].setPen();
              pageBodyList[pageIndex].clearRectanglePoints();
              pageBodyList[pageIndex].rectangleChosenFalse();
              setColorsWhite();
              penButtonColor = buttonPressedColor;
            });
          }
      ),
    );
  }
  Widget fileButton(){
    return new ButtonTheme(
      child: new IconButton(
          icon:Icon(Icons.photo,size: 30.0,color: fileButtonColor,),
          onPressed: () {
            setState(() {
              pageBodyList[pageIndex].getImage();
              setColorsWhite();
              fileButtonColor = buttonPressedColor;
            });
          }
      ),
    );
  }
  Widget colorButton(){
    return new ButtonTheme(
      child: new IconButton(
          icon:Icon(Icons.color_lens,size: 30.0,color:colorButtonColor),
          onPressed: () {
            setState(() {
              toggleColor();
              setColorsWhite();
              colorButtonColor = buttonPressedColor;
            });
          }
      ),
    );
  }
  Widget refreshButton(){
    return new ButtonTheme(
      child: new IconButton(
          icon:Icon(Icons.refresh,size: 30.0,color: refreshButtonColor),
          onPressed: () {
            setState(() {
              pageBodyList[0].refresh();
              setColorsWhite();
              refreshButtonColor = buttonPressedColor;
            });
          }
      ),
    );
  }
  Widget paperButton(){
    return new ButtonTheme(
      child: new IconButton(
          icon:Icon(Icons.library_books,size: 30.0,color: backGroundButtonColor ),
          onPressed: () {
            setState(() {
              togglePaper();
              setColorsWhite();
              backGroundButtonColor = buttonPressedColor;
            });
          }
      ),
    );
  }


  Widget colorCircle(Color color) {
    return GestureDetector(
      onTap: () {
        setState(() {
          pageBodyList[pageIndex].setColor(color);
        });
      },
      child: ClipOval(
        child: Container(
          padding: const EdgeInsets.only(bottom: 16.0),
          height: 36,
          width: 36,
          color: color,
        ),
      ),
    );
  }
  getColorList() {
    List<Widget> listWidget = List();
    for (Color color in colors) {
      listWidget.add(colorCircle(color));
    }
    Widget colorPicker = GestureDetector(             // 색깔 선택창에 대한 Widget
      onTap: () {
        showDialog(
          context: context,
          // ignore: deprecated_member_use
          child: AlertDialog(
            title: const Text('Pick a color!'),
            content: SingleChildScrollView(
              child: ColorPicker(
                pickerColor: pickerColor,
                onColorChanged: (color) {
                  pickerColor = color;
                },
                enableLabel: true,
                pickerAreaHeightPercent: 0.8,
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: const Text('Save'),
                onPressed: () {
                  setState(() => pageBodyList[0].setColor(pickerColor));
                  Navigator.of(context, rootNavigator: true).pop('dialog');
                },
              ),
            ],
          ),
        );
      },
      child: ClipOval(
        child: Container(
          padding: const EdgeInsets.only(bottom: 16.0),
          height: 36,
          width: 36,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [Colors.red, Colors.green, Colors.blue],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
              )),
        ),
      ),
    );
    listWidget.add(colorPicker);
    return listWidget;
  }

  List<DrawingPoints> temp = List();
  List<List<DrawingPoints>> listenPagePointList = [List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),List(),];




  void togglePaper(){
    paperChoose = paperChoose ? false : true;
  }

  void toggleColor(){
    colorChoose = colorChoose? false: true;
  }
  void togglePageChoose(){
    pageChoose = pageChoose? false: true;
  }

  void decode(String data, List<DrawingPoints> temp) {  // data를 받아서 temp로 만듬.
    temp.clear();
    List<String> datalist = data.split("|");
    int length = datalist.length;
    temp.clear();
    for (int i = 0; i < length - 1; i++) {
      if (datalist[i] == "null") {
        temp.add(null);
        continue;
      }
      else {
        List<String> datalist2 = datalist[i].split("*");
        int color = int.parse(datalist2[0].substring(6,16));
        double strokeWidth = double.parse(datalist2[1]);
        List<String> datalist3 = datalist2[2].split(",");
        double dx = double.parse(datalist3[0]);
        double dy = double.parse(datalist3[1]);
        double size;

        if (datalist2[3] == "null") {
          size = null;
        }
        else {
          size = double.parse(datalist2[3]);
        }
        RenderBox renderBox = context.findRenderObject();

        temp.add(DrawingPoints(
          points: renderBox.globalToLocal(Offset(dx*phoneWidth, dy*phoneHeight)),
          paint: Paint()
            ..strokeCap = strokeCap
            ..isAntiAlias = true
            ..color = Color(color)
            ..strokeWidth = strokeWidth,
          size: size,
        ));
      }
    }
  }

  void decodeAll(var data , List<List<DrawingPoints>> listenPagePointList){
    for(int i = 0 ; i < data.length ; i++){
      if( data[i] == null){
        continue;
      }
      decode(data[i],listenPagePointList[i]);
    }
  }


}

enum SelectedMode { StrokeWidth, Opacity, Color }


class StackExample extends StatefulWidget {
  final WebSocketChannel wschannel;
  Stream stream;
  final Function notify;

  StackExample({Key key, this.wschannel, this.notify}) : super(key: key);


  @override
  createState() => new StackExampleState();
}

class StackExampleState extends State<StackExample>{
  List<String> users;
  int _length ;

  @override
  void initState() {
    super.initState();
    users = [];
    _length = 0;
//    _length = users.length;
  }

  @override
  Widget build(BuildContext context) {

    Future<List<String>> waitsnapshot(AsyncSnapshot snapshot) async {
      await jsonDecode(snapshot.data)['users'].then((list){
        setState((){
          users = list;
          debugPrint('userlist: $list');
        });
      });
      return users;
    }



    debugPrint("mediaquery ${MediaQuery.of(context).size.width*8/9}");
    return new Container(
        padding: const EdgeInsets.all(8.0),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        // alignment: FractionalOffset.center,
        child:new StreamBuilder(
          stream: widget.wschannel.stream,
          builder: (context, snapshot) {
            if(snapshot.hasData){
              widget.notify;
              debugPrint('snapshot: $snapshot');

              var tmplist = jsonDecode(snapshot.data)['users'];
              users = [];
              for(int i=0; i<tmplist.length; ++i){
                var o =tmplist[i];
                users.add(o.toString().split('|').toList()[0]);
              }
              _length = users.length;

              debugPrint('length list: $users');
              debugPrint('length: $_length');
              if(_length==0){
                return new Stack(
                    alignment:new Alignment(0, 0),
                    children: <Widget>[

                      new Positioned(
                          left: 20,//
                          bottom: 50,
                          child: new Text("Users")
                      ),
                    ]
                );
              }

              else if(_length==1){
                return new Stack(
                    alignment:new Alignment(0, 0),
                    children: <Widget>[

//                      new Positioned(
//                          left: 20,//
//                          top: 100,
//                          child: new Text("Users")
//                      ),
                      new Positioned(
                          right: 0,//
                          top: 160,
                          child: new Container(
                              decoration: new BoxDecoration(
                                  border: new Border.all(
                                    color: Colors.grey,
                                    width: 0.0,
                                  ),
                                  borderRadius: new BorderRadius.all(Radius.circular(24.0),),
                                  boxShadow: [
                                    new BoxShadow(
                                      blurRadius: 5.0,
                                      color: Colors.grey,
                                      offset: new Offset(2.0, 4.0),
                                    )
                                  ]
                              ),
                              child:CircleAvatar(backgroundColor: Color.fromRGBO(252, 230, 120, 1),radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 16, child: new Text(users[0][users[0].length-1], style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),)))
                          )),

                    ]
                );
              }
              else if(_length==2){
                return new Stack(
                    alignment:new Alignment(0, 0),
                    children: <Widget>[

                      new Positioned(
                          right: 0,//
                          top: 160,
                          child: new Container(
                              decoration: new BoxDecoration(
                                  border: new Border.all(
                                    color: Colors.grey,
                                    width: 0.0,
                                  ),
                                  borderRadius: new BorderRadius.all(Radius.circular(24.0),),
                                  boxShadow: [
                                    new BoxShadow(
                                      blurRadius: 5.0,
                                      color: Colors.grey,
                                      offset: new Offset(2.0, 4.0),
                                    )
                                  ]
                              ),
                              child:CircleAvatar(backgroundColor: Color.fromRGBO(252, 200, 140, 1),radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 16, child: new Text(users[1][users[1].length-1], style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),)))
                          )),
                      new Positioned(
                          right: 25,//
                          top: 160,
                          child: new Container(
                              decoration: new BoxDecoration(
                                  border: new Border.all(
                                    color: Colors.grey,
                                    width: 0.0,
                                  ),
                                  borderRadius: new BorderRadius.all(Radius.circular(24.0),),
                                  boxShadow: [
                                    new BoxShadow(
                                      blurRadius: 5.0,
                                      color: Colors.grey,
                                      offset: new Offset(2.0, 4.0),
                                    )
                                  ]
                              ),
                              child:CircleAvatar(backgroundColor: Color.fromRGBO(252, 230, 120, 1),radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 16, child: new Text(users[0][users[0].length-1], style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),)))
                          )),

                    ]
                );
              }
              else if(_length ==3){
                return new Stack(
                    alignment:new Alignment(0, 0),
                    children: <Widget>[

                      new Positioned(
                          right: 0,//
                          top: 160,
                          child: new Container(
                              decoration: new BoxDecoration(
                                  border: new Border.all(
                                    color: Colors.grey,
                                    width: 0.0,
                                  ),
                                  borderRadius: new BorderRadius.all(Radius.circular(24.0),),
                                  boxShadow: [
                                    new BoxShadow(
                                      blurRadius: 5.0,
                                      color: Colors.grey,
                                      offset: new Offset(2.0, 4.0),
                                    )
                                  ]
                              ),
                              child:CircleAvatar(backgroundColor: Color.fromRGBO(252, 180, 160, 1),radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 16, child: new Text(users[2][users[2].length-1], style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),)))
                          )),
                      new Positioned(
                          right: 20,//
                          top: 160,
                          child: new Container(
                              decoration: new BoxDecoration(
                                  border: new Border.all(
                                    color: Colors.grey,
                                    width: 0.0,
                                  ),
                                  borderRadius: new BorderRadius.all(Radius.circular(24.0),),
                                  boxShadow: [
                                    new BoxShadow(
                                      blurRadius: 5.0,
                                      color: Colors.grey,
                                      offset: new Offset(2.0, 4.0),
                                    )
                                  ]
                              ),
                              child:CircleAvatar(backgroundColor: Color.fromRGBO(252, 200, 140, 1),radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 16, child: new Text(users[1][users[1].length-1], style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),)))
                          )),
                      new Positioned(
                          right: 40,//
                          top: 160,
                          child: new Container(
                              decoration: new BoxDecoration(
                                  border: new Border.all(
                                    color: Colors.grey,
                                    width: 0.0,
                                  ),
                                  borderRadius: new BorderRadius.all(Radius.circular(24.0),),
                                  boxShadow: [
                                    new BoxShadow(
                                      blurRadius: 5.0,
                                      color: Colors.grey,
                                      offset: new Offset(2.0, 4.0),
                                    )
                                  ]
                              ),
                              child:CircleAvatar(backgroundColor: Color.fromRGBO(252, 230, 120, 1),radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 16, child: new Text(users[0][users[0].length-1], style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),)))
                          )),

                    ]
                );
              }
              else {
                return new Stack(
                    alignment:new Alignment(0, 0),
                    children: <Widget>[


                      new Positioned(
                          right: 0,//
                          top: 160,
                          child: new Container(
                              decoration: new BoxDecoration(
                                  border: new Border.all(
                                    color: Colors.grey,
                                    width: 0.0,
                                  ),
                                  borderRadius: new BorderRadius.all(Radius.circular(24.0),),
                                  boxShadow: [
                                    new BoxShadow(
                                      blurRadius: 5.0,
                                      color: Colors.grey,
                                      offset: new Offset(2.0, 4.0),
                                    )
                                  ]
                              ),
                              child:CircleAvatar(backgroundColor: Color.fromRGBO(252, 180, 160, 1),radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 16, child: new Text(users[1][users[1].length-1], style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),)))
                          )),
                      new Positioned(
                          right: 20,//
                          top: 160,
                          child: new Container(
                              decoration: new BoxDecoration(
                                  border: new Border.all(
                                    color: Colors.grey,
                                    width: 0.0,
                                  ),
                                  borderRadius: new BorderRadius.all(Radius.circular(24.0),),
                                  boxShadow: [
                                    new BoxShadow(
                                      blurRadius: 5.0,
                                      color: Colors.grey,
                                      offset: new Offset(2.0, 4.0),
                                    )
                                  ]
                              ),
                              child:CircleAvatar(backgroundColor: Color.fromRGBO(252, 200, 140, 1),radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 16, child: new Text(users[1][users[1].length-1], style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),)))
                          )),
                      new Positioned(
                          right: 40,//
                          top: 160,
                          child: new Container(
                              decoration: new BoxDecoration(
                                  border: new Border.all(
                                    color: Colors.grey,
                                    width: 0.0,
                                  ),
                                  borderRadius: new BorderRadius.all(Radius.circular(24.0),),
                                  boxShadow: [
                                    new BoxShadow(
                                      blurRadius: 5.0,
                                      color: Colors.grey,
                                      offset: new Offset(2.0, 4.0),
                                    )
                                  ]
                              ),
                              child:CircleAvatar(backgroundColor: Color.fromRGBO(252, 230, 120, 1),radius: 20, child: new CircleAvatar(backgroundColor: Colors.white,radius: 16, child: new Text(users[0][users[0].length-1], style: TextStyle(color: Colors.grey, fontWeight: FontWeight.bold),)))
                          )),
                      new Positioned(
                        right: 60,//
                        top: 170,
                        child: new Text('+ more...', style: TextStyle(fontSize: 10, letterSpacing: 3),),
                      )
                    ]);
              }}
            else{return new Stack(
            );}
          },
        ));

  }
}
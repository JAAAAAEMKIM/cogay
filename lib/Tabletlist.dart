import 'package:flutter/material.dart';
import 'package:drawing_space/inputform.dart';

import 'package:drawing_space/Fileio.dart';
import 'package:drawing_space/GridView.dart';
import 'package:drawing_space/http.dart';
import 'package:dio/dio.dart';

import 'dart:convert';
import 'swipe_widget.dart';
import 'http.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:web_socket_channel/status.dart' as status;

import 'package:flutter_dotenv/flutter_dotenv.dart';

class TodoApp extends StatelessWidget {
  final Response res;
  final String userid;
  String username;
  final WebSocketChannel wschannel;

  TodoApp({Key key, @required this.wschannel, this.username, @required this.userid, @required this.res}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return new Container(
        child: new TodoList(resp: this.res, USERID: this.userid, wschannel: this.wschannel, username: this.username)
    );
  }
}

class TodoList extends StatefulWidget {
  final String USERID;
  Response resp;
  String username;
  final WebSocketChannel wschannel;

  TodoList({Key key, @required this.USERID, @required this.wschannel, this.username, @required this.resp}) : super(key: key);


  @override
  createState() => new TodoListState();
}

class TodoListState extends State<TodoList> {
  List<String> _todoItems=[];

  List<String> _channelhosts=[];
  List<String> _channelhosts_id=[];
  List<String> _channelcodes=[];
  Response _response;
  String Channel_;
  String USER_ID = '';
  MyHttpRequests _http = MyHttpRequests();
  bool _validate;
  String errorMessage='';

  //리스트는 로컬에 저장한 후 받아와야한다.
  // This will be called each time the + button is pressed
//  final Fileio storage = new Fileio();

  @override
  void initState(){
    super.initState();
    _validate = false;
    USER_ID = widget.USERID;
    _response = widget.resp;//, widget.USERID); //get Channels by ID
//    debugPrint('I\'M HERE');
//    debugPrint('res list: ${_response.data[0]['_id']}');
    if(_response.data.length!=0){
      for (var i = 0; i < _response.data.length; ++i) {
        var o = _response.data[i];
        List<String> hostAndChannel = o['channelname'].split('|').toList();

        _todoItems.add(hostAndChannel[1]);
        _channelhosts.add(hostAndChannel[0]);
        _channelhosts_id.add(o['channelcode'].split('|').toList()[0]);
        _channelcodes.add(o['channelcode'].toString());
      }
    }else{

    }

    setState(() {
      debugPrint('res list: $_todoItems\n channelhosts: $_channelhosts\n channel codes: $_channelcodes\n channelhosts id : $_channelhosts_id'  );
    });
  }

  // Build the whole list of todo items
  Widget _buildTodoList() {
    if(_response.data.length!=0){return new ListView.builder(
      padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
      itemCount: _channelcodes.length,
      itemBuilder: (context, index) {
        if(index < _todoItems.length) {
          return new OnSlide(
              items: <ActionItems>[
                new ActionItems(
                    icon: new IconButton(
                      icon: new Text('EDIT'), onPressed: () {}, color: Colors.blueAccent,),
                    onPress: (){
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return new InputForm(userid: USER_ID, channel: _channelcodes[index], index: index, items: _todoItems, notifyParent: refreshEdit);
                          }
                      );//edit name
                    },
                    backgroundColor: Colors.white),

                new ActionItems(
                    icon: new IconButton(
                      icon: new Icon(Icons.delete),onPressed: () {},color: Colors.red,),
                    onPress: (){
                      _http.putHttpRequest('checkoutchannel/${widget.USERID}/${_channelcodes[index]}');


                      setState((){
                        _todoItems.removeAt(index);
                        _channelhosts.removeAt(index);
                        _channelhosts_id.removeAt(index);
                        _channelcodes.removeAt(index);
                        debugPrint('after remove: $_todoItems');});
                    },
                    backgroundColor: Colors.white),
//              new ActionItems(icon: new IconButton( icon: new Icon(Icons.save),  onPressed: () {},color: Colors.blue,
//              ), onPress: (){},  backgroudColor: Colors.white),

              ],
              child: new Container(
                key: Key(_channelcodes[index]),
                child:
                ListTile(
                  onTap: () async {
                    String CHANNEL_CODE = _channelcodes[index]; //ch_code = id|ch_name //
                    _sendData("connect channel main", CHANNEL_CODE, widget.username+'|'+widget.USERID);
                    debugPrint('index $index');
                    debugPrint('list wschannel : ${widget.wschannel}');
                    Response res =  await _http.makeHttpRequest('showroom/$CHANNEL_CODE'); //get Rooms by channel code
                    debugPrint('connect channel, $CHANNEL_CODE, ${widget.username}');

                    Navigator.push(
                        context,
                        MyCustomRoute(builder: (context) => GridViewApp(listws: widget.wschannel, username: widget.username, userid: widget.USERID, res: res, channel: _channelcodes[index], channelname: _channelhosts[index]+'|'+_todoItems[index])));
                  },
                  title: Card(
                    elevation: 5,
                    child: Container(
                        height: 100.0,
                        child:Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Flexible(child:
                            Container(
                              height: 100,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    color: Color.fromRGBO(252, 120+index*20, 121, 1),
                                    width: 15,
                                  ),
                                  Flexible(child:
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(20, 15, 0, 0),
                                    child:  Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        new Container(
                                          padding: new EdgeInsets.only(right: 13.0),
                                          child: Text(
                                            _todoItems[index],overflow: TextOverflow.ellipsis, style: TextStyle(fontSize: 24),
                                          ),
                                        ),


                                        Padding(
                                          padding: EdgeInsets.fromLTRB(0, 3, 0, 3),
                                          child: _channelhosts_id[index] == widget.USERID
                                              ?
                                          Container(
                                            width: 30,
                                            decoration: BoxDecoration(
                                                border: Border.all(color: Colors.teal),
                                                borderRadius: BorderRadius.all(Radius.circular(10))
                                            ),
                                            child: Text("Me",textAlign: TextAlign.center),
                                          )
                                              :
                                          Container(
                                            width: 30,
                                            //                                      decoration: BoxDecoration(
                                            //                                          border: Border.all(color: Colors.redAccent),
                                            //                                          borderRadius: BorderRadius.all(Radius.circular(10))
                                            //                                      ),
                                            //                                      child: Text(_channelhosts[index],textAlign: TextAlign.center,),
                                          )
                                          ,
                                        ),
                                        //                          Divider(),
                                        Padding(
                                          padding: EdgeInsets.fromLTRB(0, 5, 0, 2),
                                          child: Container(
                                            width: 260,
                                            child: Text('Host: ${_channelhosts[index]}',
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  color: Color.fromARGB(255, 48, 48, 54)
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                  ),
                                ],
                              ),
                            ),),
                            //                    Text(_todoItems[index]),
                            Row(children:<Widget>[VerticalDivider(color: Colors.grey, width:0,),IconButton(icon: Icon(Icons.arrow_forward_ios)),]),
                            //                    Text(_channelhosts[index])
                          ],
                        )
                    ),
                  ),),
              )
          );
        }
        else return null;
      },
    );}
    else{return new Material(child:Row(crossAxisAlignment: CrossAxisAlignment.center, mainAxisAlignment:MainAxisAlignment.center, children:<Widget>[Column(mainAxisAlignment:MainAxisAlignment.center,children:<Widget>[Text("No Contents to Show")])]));}
  }

  final formkey = GlobalKey<FormState>();
  String invitecode;
  TextEditingController textEditingController = new TextEditingController();

  void _submit() {
    if(formkey.currentState.validate()) {
      formkey.currentState.save();
      debugPrint('aaaaaaaaaaaaaa $invitecode');
      setState(() {
        debugPrint('aaaabbbbbb $invitecode');
//        invitecode = val;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    String submitStr = "";


    void _onSubmit(String val) {
      print("OnSubmit : The text is $val");
      setState(() {
        submitStr = val;
      });
    }

    void _onChanged(String value) {
      print('"OnChange : " $value');
    }

    return new Scaffold(
      body: _buildTodoList(),
      floatingActionButton:
      SpeedDial(
        animatedIcon: AnimatedIcons.list_view,
        animatedIconTheme: IconThemeData(size: 22.0),
        // this is ignored if animatedIcon is non null
        // child: Icon(Icons.add),
//          visible: _dialVisible,
        curve: Curves.bounceIn,
        overlayColor: Colors.black,
        overlayOpacity: 0.5,
        onOpen: () => print('OPENING DIAL'),
        onClose: () => print('DIAL CLOSED'),
        tooltip: 'Speed Dial',
        heroTag: 'speed-dial-hero-tag',
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        elevation: 8.0,
        shape: CircleBorder(),
        children: [
          SpeedDialChild(
            child: Icon(Icons.add_box, color: Colors.white,),
            backgroundColor: Colors.orangeAccent,
            label: '새 폴더',
//                labelStyle: TextTheme(fontSize: 18.0),
            onTap: (){
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    int index = 1000007;
                    return new InputForm(userid: USER_ID, channel: null, index: index, items: _todoItems, notifyParent: refreshAdd);
                  }
              );//edit name

            },
          ),
          SpeedDialChild(
            child: Icon(Icons.local_post_office, color: Colors.white,),
            backgroundColor: Colors.deepOrangeAccent,
            label: '초대 코드',
//              labelStyle: TextTheme(fontSize: 18.0),
            onTap: () async {
              showDialog(
                  context: context,
                  builder:(_)=> new Container(
                      child:AlertDialog(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(Radius.circular(24.0))),
                          content: new Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text('Invite Code'),
                                TextField(
                                  controller: textEditingController = TextEditingController(),
                                  autofocus: true,
                                  decoration: new InputDecoration(
                                    hintText: 'Enter Code Here',
                                    contentPadding: const EdgeInsets.all(16.0),
                                    errorText: _validate ? errorMessage : null,
                                  ),
                                  onChanged: (String value) {
                                    _onChanged(value);
                                  },
                                  onSubmitted: (String submittedStr) {
                                    _onSubmit(submittedStr);
                                    textEditingController.text = "";
                                  },

                                ),
                                new Text('$submitStr'),
                                new RaisedButton(
                                    color: Colors.orangeAccent,
                                    shape: RoundedRectangleBorder(borderRadius: new BorderRadius.circular(15.0)),
                                    child: new Text("Submit", style: TextStyle(color: Colors.white),),
                                    onPressed: () {
//                                        _changeText(textEditingController.text);
                                      String val = textEditingController.text;
                                      if(val.split('|').length !=3){
                                        _validate=true;
                                        setState(() {
                                          errorMessage = "Invalid Code";
                                          debugPrint('$errorMessage');
                                        });
                                      }
                                      else if(checkInviteVal(val.split('/')[0], _channelcodes)) {
                                        _http.putHttpRequest('giveauth/${widget.USERID}/$val');
                                        Navigator.of(context, rootNavigator: true).pop('dialog');
                                        setState(() {
                                          _validate=false;

                                          errorMessage = "";
                                          debugPrint('$_validate');
                                        });
                                      }else{
                                        _validate=true;
                                        setState(() {
                                          errorMessage = "Folder Already Exists";
                                          debugPrint('$_validate');
                                        });
                                      }
                                    }

                                )
                              ]
                          )
                      )
                  )
              );
            },
          ),
        ],
      ),
    );

  }

  refreshEdit(String val, int index) async{
    Response tmp = await _http.makeHttpRequest('/showchannel/$USER_ID').then((tmp){
      _response = tmp;
      setState(() {
        _todoItems.removeAt(index);
        _todoItems.insert(index,val);
//        _channelhosts.removeAt(index);
//        _channelhosts_id.removeAt(index);
//        _channelcodes.removeAt(index);
        debugPrint('after remove: $_todoItems');
      });
      return null;
    });
  }
  refreshAdd(String val, int index)async{
    Response tmp = await _http.makeHttpRequest('/showchannel/$USER_ID').then((tmp){
      _response = tmp;
      setState(() {
        var o = _response.data[_response.data.length-1];
        List<String> hostAndChannel = o['channelname'].split('|').toList();

        _todoItems.add(hostAndChannel[1]);
        _channelhosts.add(hostAndChannel[0]);
        _channelhosts_id.add(o['channelcode'].split('|').toList()[0]);
        _channelcodes.add(o['channelcode'].toString());
      });

      return null;
    });
  }

  bool checkInviteVal(String val, List<String> list){
    bool answer = true;
    if(val.length==0){
      answer = false;
    }
    else if(ValAlreadyinList(val, list)){
      answer = false;
    }
    debugPrint('answer:$answer');
    return answer;
  }
  bool ValAlreadyinList(String val, List<String> list){
    bool answer = false;
    for (var i = 0; i < list.length; ++i) {
      var o = list[i];

      if(val == o){
        answer = !answer;
      }
    }
    return answer;
  }


  void _sendData(String event, String channel, String username) {
//    debugPrint('websocket: ${widget.wschannel}');
    widget.wschannel.sink.add(
        jsonEncode({
          'event': event,
          'channel': channel,
          'room': '',
          'data': '',
          'nickname': username, //nickname + id
          'page':0,
        })
    );
  }
}

class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({ WidgetBuilder builder, RouteSettings settings })
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      Widget child) {
    if (settings.isInitialRoute)
      return child;

    return new  FadeTransition(opacity: animation, child: child);
  }
}
